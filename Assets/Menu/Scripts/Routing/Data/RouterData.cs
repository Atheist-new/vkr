using Menu.Scripts.Pages;

namespace Menu.Scripts.Routing.Data
{
    public class RouterData
    {
        public string Route;
        public string Alias;
        public Page Page;

        public RouterData(string route, string alias, Page page)
        {
            Route = route;
            Alias = alias;
            Page = page;
        }
    }
}