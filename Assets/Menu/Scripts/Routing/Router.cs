using System;
using System.Collections.Generic;
using Menu.Scripts.Pages;
using Menu.Scripts.Routing.Data;
using UnityEngine;
using System.Linq;
using Extensions;

namespace Menu.Scripts.Routing
{
    public class Router : MonoBehaviour
    {
        public static Router Main { get; private set; }

        public PageList pageList;

        private List<RouterData> _routes;

        private Page _currentPage;
        private List<Page> _backPages = new();

        private void Start()
        {
            _routes = new List<RouterData>
            {
                new ("", "StartMenu", pageList.StartMenu),
                new ("", "PatternMenu", pageList.PatternMenu),
                new ("", "StorylineMenu", pageList.StorylineMenu),
                new ("", "ArmyMenu", pageList.ArmyMenu),
                new ("", "SoldiersMenu", pageList.SoldiersMenu),
                new ("", "SquadsMenu", pageList.SquadsMenu),
                new ("", "CreateSquadMenu", pageList.CreateSquadsMenu),
                new ("", "GeneralsMenu", pageList.GeneralsMenu),
                new ("", "OfficersMenu", pageList.OfficersMenu),
                new ("", "CreateOfficerMenu", pageList.CreateOfficerMenu),
                new ("", "CreateSoldierMenu", pageList.CreateSoldierMenu),
                new ("", "CreateGeneralMenu", pageList.CreateGeneralMenu),
                new ("", "SelectArmyMenu", pageList.SelectArmyMenu),
                new ("", "CreateArmyMenu", pageList.CreateArmyMenu),
                new ("", "FormationArmyMenu", pageList.FormationArmyMenu)
            };

            Main = this;
            
            To("StartMenu");
        }

        public void To(string alias)
        { 
            var page = _routes.First(item => item.Alias == alias);
            if (_currentPage != null)
            {
                _currentPage.Hide();
                _backPages.Add(_currentPage);
            }
            
            _currentPage = page.Page;
            _currentPage.Show();
        }

        public void Back()
        {
            try
            {
                var lastPage = _backPages.Pop();
                _currentPage.Hide();
                lastPage.Show();

                _currentPage = lastPage;
            }
            catch (IndexOutOfRangeException)
            {
                To("StartMenu");
            }
        }
    }
}