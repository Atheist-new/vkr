using System.Collections.Generic;
using TMPro;
using Types.DTOs;
using UnityEngine;
using UnityEngine.UI;

namespace Menu.Scripts.Repositories
{
    public class Repository
    {
        public List<SoldierDto> SoldiersData;
        public List<OfficersDto> OfficersData;
        public List<GeneralsDto> GeneralsData;
        public List<SquadsDto> SquadsData;
        private static Repository instance;
        public static Repository Instance => instance ??= new Repository();

        public Repository()
        {
            //Данные солдат
            SoldiersData = new List<SoldierDto>{
                new (1, "Солдат света", 900, 1700, 100, 75, 
                    1200, 700, 550, 200, 100, Color.yellow, Color.white),
                new (2, "Солдат тьмы", 800, 1400, 80, 60, 
                    900, 550, 200, 250, 200, Color.magenta, Color.black),
                new (3, "Солдат пламени", 500, 1200, 65, 65, 
                    760, 320, 125, 300, 60, Color.red, Color.cyan),
                new (4, "Солдат братства", 280, 145, 40, 30, 
                    600, 295, 50, 110, 40, Color.green, Color.blue),
            };
            //Данные офицеров
            OfficersData = new List<OfficersDto>
            {
                new(1, "Офицер света", 1900, 3700, 200, 150,
                    11200, 4000, 3500, 350, 250, 100000),
                new(2, "Офицер тьмы", 1400, 3000, 180, 130,
                    9900, 3200, 2900, 310, 200, 80000),
                new(3, "Офицер пламени", 1225, 2870, 165, 120,
                    8400, 2995, 2545, 290, 150, 60000),
            };
            //Данные генералов
            GeneralsData = new List<GeneralsDto>
            {
                new(1, "Генерал света", 1900, 3700, 200, 150,
                    11200, 4000, 3500, 350, 250),
                new(2, "Генерал тьмы", 1400, 3000, 180, 130,
                    9900, 3200, 2900, 310, 200),
            };
            //Данные отрядов
            SquadsData = new List<SquadsDto>
            {
                
            };
        }
        //
        public SoldierDto FindSoldier(int soldierID)
        {
            return SoldiersData.Find(soldier => soldier.ID == soldierID);
        }
        //
        public OfficersDto FindOfficer(int officerID)
        {
            return OfficersData.Find(officer => officer.ID == officerID);
        }
        //
        public GeneralsDto FindGeneral(int generalID)
        {
            return GeneralsData.Find(general => general.ID == generalID);
        }
        //
        public SquadsDto FindSquads(int squadID)
        {
            return SquadsData.Find(squad  => squad.IdSquad == squadID);
        }

        public void SelectSoldier(GameObject characteristics, SoldierDto soldierDto, GameObject magicPanel)
        {
            characteristics.transform.Find("PhysicalDamage/PhysicalDamage_Indicator").GetComponent<TextMeshProUGUI>().text = soldierDto.PhysicalDamage.ToString();
            characteristics.transform.Find("MagicDamage/MagicDamage_Indicator").GetComponent<TextMeshProUGUI>().text = soldierDto.MagicDamage.ToString();
            characteristics.transform.Find("AttackSpeed/AttackSpeed_Indicator").GetComponent<TextMeshProUGUI>().text = soldierDto.AttackSpeed.ToString();
            characteristics.transform.Find("AttackRange/AttackRange_Indicator").GetComponent<TextMeshProUGUI>().text = soldierDto.AttackRange.ToString();
            characteristics.transform.Find("Health/Health_Indicator").GetComponent<TextMeshProUGUI>().text = soldierDto.Health.ToString();
            characteristics.transform.Find("Protection/Protection_Indicator").GetComponent<TextMeshProUGUI>().text = soldierDto.Protection.ToString();
            characteristics.transform.Find("MagicalProtection/MagicalProtection_Indicator").GetComponent<TextMeshProUGUI>().text = soldierDto.MagicalProtection.ToString();
            characteristics.transform.Find("MoveSpeed/MoveSpeed_Indicator").GetComponent<TextMeshProUGUI>().text = soldierDto.MoveSpeed.ToString();
            characteristics.transform.Find("LoadCapacity/LoadCapacity_Indicator").GetComponent<TextMeshProUGUI>().text = soldierDto.LoadCapacity.ToString();

            magicPanel.transform.Find("Skill1").GetComponent<Image>().color = soldierDto.MagicSkill1;
            magicPanel.transform.Find("Skill2").GetComponent<Image>().color = soldierDto.MagicSkill2;
        }

        public void SelectOfficer(GameObject characteristics, OfficersDto officersDto, GameObject squadSizeIndicator)
        {
            characteristics.transform.Find("PhysicalDamage/PhysicalDamage_Indicator").GetComponent<TextMeshProUGUI>().text = officersDto.PhysicalDamage.ToString();
            characteristics.transform.Find("MagicDamage/MagicDamage_Indicator").GetComponent<TextMeshProUGUI>().text = officersDto.MagicDamage.ToString();
            characteristics.transform.Find("AttackSpeed/AttackSpeed_Indicator").GetComponent<TextMeshProUGUI>().text = officersDto.AttackSpeed.ToString();
            characteristics.transform.Find("AttackRange/AttackRange_Indicator").GetComponent<TextMeshProUGUI>().text = officersDto.AttackRange.ToString();
            characteristics.transform.Find("Health/Health_Indicator").GetComponent<TextMeshProUGUI>().text = officersDto.Health.ToString();
            characteristics.transform.Find("Protection/Protection_Indicator").GetComponent<TextMeshProUGUI>().text = officersDto.Protection.ToString();
            characteristics.transform.Find("MagicalProtection/MagicalProtection_Indicator").GetComponent<TextMeshProUGUI>().text = officersDto.MagicalProtection.ToString();
            characteristics.transform.Find("MoveSpeed/MoveSpeed_Indicator").GetComponent<TextMeshProUGUI>().text = officersDto.MoveSpeed.ToString();
            characteristics.transform.Find("LoadCapacity/LoadCapacity_Indicator").GetComponent<TextMeshProUGUI>().text = officersDto.LoadCapacity.ToString();
            
            squadSizeIndicator.GetComponent<TextMeshProUGUI>().text = officersDto.SquadSize.ToString();
        }

        public void SelectSquad(GameObject characteristics, OfficersDto officersDto)
        {
            characteristics.transform.Find("PhysicalDamage/PhysicalDamage_Indicator").GetComponent<TextMeshProUGUI>().text = (officersDto.PhysicalDamage * 3).ToString();
            characteristics.transform.Find("MagicDamage/MagicDamage_Indicator").GetComponent<TextMeshProUGUI>().text = (officersDto.MagicDamage * 3).ToString();
            characteristics.transform.Find("AttackSpeed/AttackSpeed_Indicator").GetComponent<TextMeshProUGUI>().text = (officersDto.AttackSpeed * 3).ToString();
            characteristics.transform.Find("AttackRange/AttackRange_Indicator").GetComponent<TextMeshProUGUI>().text = (officersDto.AttackRange * 3).ToString();
            characteristics.transform.Find("Health/Health_Indicator").GetComponent<TextMeshProUGUI>().text = (officersDto.Health * 3).ToString();
            characteristics.transform.Find("Protection/Protection_Indicator").GetComponent<TextMeshProUGUI>().text = (officersDto.Protection * 3).ToString();
            characteristics.transform.Find("MagicalProtection/MagicalProtection_Indicator").GetComponent<TextMeshProUGUI>().text = (officersDto.MagicalProtection * 3).ToString();
            characteristics.transform.Find("MoveSpeed/MoveSpeed_Indicator").GetComponent<TextMeshProUGUI>().text = (officersDto.MoveSpeed * 3).ToString();
            characteristics.transform.Find("LoadCapacity/LoadCapacity_Indicator").GetComponent<TextMeshProUGUI>().text = (officersDto.LoadCapacity * 3).ToString();
        }
        
        public void UnselectedCharacter(GameObject characteristics, GameObject magicPanel)
        {
            characteristics.transform.Find("PhysicalDamage/PhysicalDamage_Indicator").GetComponent<TextMeshProUGUI>().text = "-";
            characteristics.transform.Find("MagicDamage/MagicDamage_Indicator").GetComponent<TextMeshProUGUI>().text = "-";
            characteristics.transform.Find("AttackSpeed/AttackSpeed_Indicator").GetComponent<TextMeshProUGUI>().text = "-";
            characteristics.transform.Find("AttackRange/AttackRange_Indicator").GetComponent<TextMeshProUGUI>().text = "-";
            characteristics.transform.Find("Health/Health_Indicator").GetComponent<TextMeshProUGUI>().text = "-";
            characteristics.transform.Find("Protection/Protection_Indicator").GetComponent<TextMeshProUGUI>().text = "-";
            characteristics.transform.Find("MagicalProtection/MagicalProtection_Indicator").GetComponent<TextMeshProUGUI>().text = "-";
            characteristics.transform.Find("MoveSpeed/MoveSpeed_Indicator").GetComponent<TextMeshProUGUI>().text = "-";
            characteristics.transform.Find("LoadCapacity/LoadCapacity_Indicator").GetComponent<TextMeshProUGUI>().text = "-";
            
            magicPanel.transform.Find("Skill1").GetComponent<Image>().color = Color.grey;
            magicPanel.transform.Find("Skill2").GetComponent<Image>().color = Color.grey;
        }
        
        public void DestroyList(List<GameObject> listObject)
        {
            listObject.ForEach(listObject => { Object.Destroy(listObject); });      //Поэлементное удаление всех экземпляров листа объектов
            listObject.Clear();
        }
    }
}