/*using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor.Presets;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuMenager : MonoBehaviour
{
    public static List<GameObject> listObject = new();
    private GameObject toCreateGenerals;    //Откуда пришёл в CreateGenerals
    [Header("Экранные формы")]
    public GameObject startMenu;
    public GameObject selectionPatternArmy;
    public GameObject menuPatterns;
    public GameObject createPatternArmy;
    public GameObject createBuildingArmy;
    public GameObject squardSelection;
    public GameObject createGeneral;
    public GameObject equipmentSelection;
    public GameObject topBrowser;

    [Header("Префабы")]
    public GameObject itemButtonPrefab;

    [Header("Элименты экранных форм")]
    public GameObject listItems;            //Поле, в котором будут динамически создаваться кнопки предметов

    public void Start()
    {
        ToStartMenu();
    }
    public void ToStartMenu()
    {
        startMenu.SetActive(true);
        selectionPatternArmy.SetActive(false);
        menuPatterns.SetActive(false);
        createPatternArmy.SetActive(false);
        createBuildingArmy.SetActive(false);
        squardSelection.SetActive(false);
        createGeneral.SetActive(false);
        equipmentSelection.SetActive(false);
        topBrowser.SetActive(false);
    }
    public void ToMenuPatterns()
    {
        startMenu.SetActive(false);
        createPatternArmy.SetActive(false);
        createBuildingArmy.SetActive(false);
        menuPatterns.SetActive(true);
    }
    public void MenuPatternsToSelectionPatternArmy()
    {
        menuPatterns.SetActive(false);
        selectionPatternArmy.SetActive(true);
        topBrowser.SetActive(true);
    }
    public void SelectionPatternArmyToCreatePatternArmy()
    {
        selectionPatternArmy.SetActive(false);
        createPatternArmy.SetActive(true);
    }
    public void CreateBuildingArmyToCreatePatternArmy()
    {
        createBuildingArmy.SetActive(false);
        createPatternArmy.SetActive(true);
    }
    public void ToCreateBuildingArmy()
    {
        createPatternArmy.SetActive(false);
        createBuildingArmy.SetActive(true);
    }
    public void ToSquardSelection(GameObject buttonPos)
    {
        squardSelection.SetActive(true);
    }
    public void BackSquardSelection()
    {
        squardSelection.SetActive(false);
    }
    public void SelectedSquardSelection()
    {
        squardSelection.SetActive(false);
    }
    public void MenuPatternsToCreateOfficers()
    {
        menuPatterns.SetActive(false);
        toCreateGenerals = menuPatterns;
        createGeneral.SetActive(true);
    }
    public void MenuPatternsToCreateGeneral()
    {
        menuPatterns.SetActive(false);
        toCreateGenerals = menuPatterns;
        createGeneral.SetActive(true);
    }
    public void CreatePatternArmyToCreateGeneral()
    {
        createPatternArmy.SetActive(false);
        toCreateGenerals = createPatternArmy;
        createGeneral.SetActive(true);
    }
    public void BackCreateGeneral()
    {
        createGeneral.SetActive(false);
        if (toCreateGenerals == menuPatterns)
        {
            menuPatterns.SetActive(true);
        }
        else if (toCreateGenerals == createPatternArmy)
        {
            createPatternArmy.SetActive(true);
        }
    }
}
*/