using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SelectStoryline : MonoBehaviour
{
    [Header("�������� �����")]
    public GameObject storyline;
    public GameObject selectStoryline;
    public GameObject menuStoryline;
    public void BackToStoryline()
    {
        menuStoryline.SetActive(true);
        selectStoryline.SetActive(false);
    }
    public void ToMapSelectStoryline()
    {
        selectStoryline.SetActive(false);
        storyline.SetActive(false);
        SceneManager.LoadScene("Level");
    }
}
