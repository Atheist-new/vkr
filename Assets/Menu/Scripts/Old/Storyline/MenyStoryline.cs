using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenyStoryline : MonoBehaviour
{
    [Header("�������� �����")]
    public GameObject startMenu;
    public GameObject storyline;
    public GameObject selectStoryline;
    public GameObject menuStoryline;

    public void Start()
    {
    }
    public void BackToStartMenu()
    {
        startMenu.SetActive(true);
        storyline.SetActive(false);
    }
    public void ToSelectStoryline()
    {
        selectStoryline.SetActive(true);
        menuStoryline.SetActive(false);
    }
}
