using DataClass;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SelectGeneral : MonoBehaviour
{
    private static int IDGeneral;
    public static int ID_General { get => IDGeneral; }
    List<int> IDobject = new List<int>() { 0, 1, 2 };    //���� �������� �����, ��������� ������
    List<string> nameObject = new List<string>() { "������� ������", "������� ����������", "������� ������ �������" };
    public List<GameObject> listObject = new();
    [Header("�������� �����")]
    public GameObject selectionGeneral;
    public GameObject editGeneral;
    public GameObject menuPatterns;
    [Header("�������")]
    public GameObject itemButtonPrefab;
    [Header("�������� �������� ����")]
    public GameObject NameGeneral; //��� ��������
    public GameObject listItemsCharacter;   //����, � ������� ����� ����������� ����������� ������ ����������
    void OnEnable()
    {
        for (int i = 0; i < IDobject.Count; i++)  //����������� �������� ���������
        {
            int index = i;  //������� ��� ������ �����
            //�������� �������� ��� ������ �������
            UnityAction setItem = new UnityAction(() => { setSelection(index); });

            GameObject item = Instantiate(itemButtonPrefab, listItemsCharacter.transform);   //��������� itemButtonPrefab � listItems
            listObject.Add(item);    //���������� ������ item � ���� � ����������� ���������� ���������

            item.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = nameObject[index];
            item.name = "item " + (index);
            item.transform.localPosition = new Vector3(0, 280 - index * 140, 0);
            item.GetComponent<Button>().onClick.AddListener(setItem);   //���������� ���������� Button �� ��������� setItem
        }
        StartCoroutine(setInitialSelection(IDGeneral < 0 ? 0 : IDGeneral));
    }

    private IEnumerator setInitialSelection(int index)
    {
        yield return null;
        setSelection(index);
    }

    private void OnDisable()
    {
        MMStartMenu.DestroyList(listObject);
    }

    public void BackMenuPatterns()
    {
        selectionGeneral.SetActive(false);
        menuPatterns.SetActive(true);
    }

    public void ToEditGeneral()
    {
        CreateGeneral.IdToCreateGeneral = 1;
        selectionGeneral.SetActive(false);
        editGeneral.SetActive(true);
    }

    public void setSelection(int index)   //��������� �������� ������ �������
    {
        int itemID = IDobject[index];
        string name = nameObject[index];
        NameGeneral.GetComponent<TextMeshProUGUI>().text = name;
        IDGeneral = itemID;
    }
}
