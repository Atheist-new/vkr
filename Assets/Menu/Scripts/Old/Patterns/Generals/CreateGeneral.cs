using DataClass;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CreateGeneral : MonoBehaviour
{
    private static int idToCreateGeneral = 0;
    public static int IdToCreateGeneral { set => idToCreateGeneral = value; }
    private static int IDGeneral;
    List<string> nameObject = new List<string>() { "������� ������", "������� ����������", "������� ������ �������" };
    [Header("�������� �����")]
    public GameObject selectionGeneral;
    public GameObject selectionItem;
    public GameObject createPatternArmy;
    public GameObject selectMinGeneral;
    public GameObject createGeneral;
    public GameObject nameArmy; //�������� �����
    void OnEnable()
    {
        IDGeneral = SelectGeneral.ID_General;
        nameArmy.GetComponent<TextMeshProUGUI>().text = nameObject[IDGeneral];
    }

    public void CreateGeneralToSelectionItem()
    {
        selectionItem.SetActive(true);
    }

    public void Back()
    {
        if (idToCreateGeneral == 1)
        {
            selectionGeneral.SetActive(true);
        }
        else if (idToCreateGeneral == 2)
        {
            selectMinGeneral.SetActive(true);
        }
        createGeneral.SetActive(false);
    }
    public void SaveGeneral()
    {
        if (idToCreateGeneral == 1)
        {
            selectionGeneral.SetActive(true);
        }
        else if (idToCreateGeneral == 2)
        {
            createPatternArmy.SetActive(true);
        }
        createGeneral.SetActive(false);
    }
}
