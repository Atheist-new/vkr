using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SelectionSquadMin : MonoBehaviour
{
    private int ID = -1;
    public static int index = -1;
    List<int> IDobject = new List<int>() { 1, 2, 3 };    //���� �������� �����, ��������� ������
    List<string> nameObject = new List<string>() { "������� �����", "������� ����", "������� �����" };
    public List<GameObject> listObject = new();
    [Header("�������� �����")]
    public GameObject selectionSquad;
    public GameObject createBuildingArmy;
    [Header("�������")]
    public GameObject itemButtonPrefab;
    [Header("�������� �������� ����")]
    public List<GameObject> posSquar; //������� ������� (1-5 �����, 6-7 ��, 8-9 ��)
    public GameObject listItemsSquad;   //����, � ������� ����� ����������� ����������� ������ ����������
    public GameObject nameSquad;   //���� �������� ������
    void OnEnable()
    {
        index = -1;
        for (int i = 0; i < IDobject.Count; i++)  //����������� �������� ���������
        {
            int index = i;  //������� ��� ������ �����
            //�������� �������� ��� ������ �������
            UnityAction setItem = new UnityAction(() => { setSelection(IDobject[index], nameObject[index]); });

            GameObject item = Instantiate(itemButtonPrefab, listItemsSquad.transform);   //��������� itemButtonPrefab � listItems
            listObject.Add(item);    //���������� ������ item � ���� � ����������� ���������� ���������

            item.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = nameObject[index];
            item.name = "item " + (index + 1);
            item.transform.localPosition = new Vector3(0, 280 - index * 140, 0);
            item.GetComponent<Button>().onClick.AddListener(setItem);   //���������� ���������� Button �� ��������� setItem
        }
    }
    private void OnDisable()
    {
        MMStartMenu.DestroyList(listObject);
    }


    public void BackCreatePatternArmy()
    {
        selectionSquad.SetActive(false);
        createBuildingArmy.SetActive(true);
    }
    public void SelectSquard()
    {
        if(CreateBuildingArmy.num < 5 && index != -1)
        {
            posSquar[index -1].transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = nameSquad.GetComponent<TextMeshProUGUI>().text;
            if (CreateBuildingArmy.flag[index-1])
            {
                CreateBuildingArmy.num++;
                CreateBuildingArmy.flag[index] = true;
            }
        }
        selectionSquad.SetActive(false);
        createBuildingArmy.SetActive(true);
    }


    public void setSelection(int itemID, string name)   //��������� �������� ������ �������
    {
        nameSquad.GetComponent<TextMeshProUGUI>().text = name;
        ID = itemID;
    }
}
