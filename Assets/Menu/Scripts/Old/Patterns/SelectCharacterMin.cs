using DataClass;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SelectCharacterMin : MonoBehaviour
{
    public static int type = - 1;
    int ID = -1;
    List<int> IDobject = new List<int>() { 1, 2, 3 };    //Лист индексов армий, доступных игроку
    List<string> nameObject = new List<string>() { "Генерал света", "Генерал огня", "Генерал ветра" };
    public List<GameObject> listObject = new();
    [Header("Экранные формы")]
    public GameObject createPatternArmy;
    public GameObject selectionGeneral;
    public GameObject editCharacter;
    [Header("Префабы")]
    public GameObject itemButtonPrefab;
    [Header("Элименты экранных форм")]
    public GameObject generalArmy; //Имя генерала
    public GameObject generalArmy2; //Имя генерала
    public GameObject generalArmy3; //Имя генерала
    public GameObject listItemsCharacter;   //Поле, в котором будет динамически создаваться список персонажей
    public GameObject skill5;
    public GameObject skill6;
    void OnEnable()
    {
        if(type == 0)
        {
            for (int i = 0; i < IDobject.Count; i++)  //Циклическое создание элементов
            {
                int index = i;  //Костыль для работы лямды
                                //Создание свойства для нового объекта
                UnityAction setItem = new UnityAction(() => { setSelection(IDobject[index], nameObject[index]); });

                GameObject item = Instantiate(itemButtonPrefab, listItemsCharacter.transform);   //Установка itemButtonPrefab в listItems
                listObject.Add(item);    //Добавление нового item в лист с динамически созданными объектами

                item.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = nameObject[index];
                item.name = "item " + (index + 1);
                item.transform.localPosition = new Vector3(0, 280 - index * 140, 0);
                item.GetComponent<Button>().onClick.AddListener(setItem);   //Добавление компонента Button со свойством setItem
            }
        }

    }
    private void OnDisable()
    {
        MMStartMenu.DestroyList(listObject);
    }

    public void BackCreatePatternArmy()
    {
        selectionGeneral.SetActive(false);
        createPatternArmy.SetActive(true);
    }

    public void SelectionGeneral()
    {
        if (ID != -1)
        {
            generalArmy.GetComponent<TextMeshProUGUI>().text = nameObject[ID - 1];
            selectionGeneral.SetActive(false);
            createPatternArmy.SetActive(true);
        }
    }

    public void ToEditCharacter()
    {
        if(ID != -1) 
        {
            selectionGeneral.SetActive(false);
            createPatternArmy.SetActive(false);
            editCharacter.SetActive(true);
            generalArmy3.GetComponent<TextMeshProUGUI>().text = nameObject[ID - 1];
        }
    }


    public void setSelection(int itemID, string name)   //Установка механики выбора объекта
    {
        generalArmy2.GetComponent<TextMeshProUGUI>().text = name;
        ID = itemID;
    }
}
