using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateUnit : MonoBehaviour
{
    public GameObject selectionUnit;
    public GameObject createUnit;

    public void Back()
    {
        selectionUnit.SetActive(true);
        createUnit.SetActive(false);
    }
    
}
