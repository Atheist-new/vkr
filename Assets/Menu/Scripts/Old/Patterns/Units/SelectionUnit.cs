using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SelectionUnit : MonoBehaviour
{
    int ID;
    List<int> IDobject = new List<int>() { 1, 2, 3 };    //���� �������� �����, ��������� ������
    List<string> nameObject = new List<string>() { "������ �����", "������ ����", "������ �����" };
    public List<GameObject> listObject = new();
    [Header("�������� �����")]
    public GameObject selectionUnit;
    public GameObject createUnit;
    public GameObject menuPatterns;
    [Header("�������")]
    public GameObject itemButtonPrefab;
    [Header("�������� �������� ����")]
    public GameObject unitName; //��� ��������
    public GameObject unitName2; //��� ��������
    public GameObject listItemsUnit;   //����, � ������� ����� ����������� ����������� ������ ����������

    void OnEnable()
    {
        for (int i = 0; i < IDobject.Count; i++)  //����������� �������� ���������
        {
            int index = i;  //������� ��� ������ �����
                            //�������� �������� ��� ������ �������
            UnityAction setItem = new UnityAction(() => { setSelection(IDobject[index], nameObject[index]); });

            GameObject item = Instantiate(itemButtonPrefab, listItemsUnit.transform);   //��������� itemButtonPrefab � listItems
            listObject.Add(item);    //���������� ������ item � ���� � ����������� ���������� ���������

            item.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = nameObject[index];
            item.name = "item " + (index + 1);
            item.transform.localPosition = new Vector3(0, 280 - index * 140, 0);
            item.GetComponent<Button>().onClick.AddListener(setItem);   //���������� ���������� Button �� ��������� setItem
        }
    }
    private void OnDisable()
    {
        MMStartMenu.DestroyList(listObject);
    }

    public void BackMenuPatterns()
    {
        selectionUnit.SetActive(false);
        menuPatterns.SetActive(true);
    }

    public void ToEditUnit()
    {
        if (ID != -1)
        {
            // createUnit.SetActive(true);
            // selectionUnit.SetActive(false);
            // unitName2.GetComponent<TextMeshProUGUI>().text = nameObject[ID - 1];
        }
    }


    public void setSelection(int itemID, string name)   //��������� �������� ������ �������
    {
        unitName.GetComponent<TextMeshProUGUI>().text = name;
        ID = itemID;
    }
}
