using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SelectionSquad : MonoBehaviour
{
    int ID;
    List<int> IDobject = new List<int>() { 1, 2, 3 };    //���� �������� �������, ��������� ������
    List<string> nameObject = new List<string>() { "����� �������� �����", "����� �������� ����", "����� �������� �����" };
    public List<GameObject> listObject = new();
    [Header("�������� �����")]
    public GameObject selectionSquad;
    public GameObject editSquad;
    public GameObject menuPatterns;
    public GameObject createBuildingArmy;
    [Header("�������")]
    public GameObject itemButtonPrefab;
    [Header("�������� �������� ����")]
    public GameObject squadName; //��� ������
    public GameObject squadName2; //��� ������ (��������)
    public GameObject listItemsUnit;   //����, � ������� ����� ����������� ����������� ������ �������

    void OnEnable()
    {
        for (int i = 0; i < IDobject.Count; i++)  //����������� �������� ���������
        {
            int index = i;  //������� ��� ������ �����
                            //�������� �������� ��� ������ �������
            UnityAction setItem = new UnityAction(() => { setSelection(IDobject[index], nameObject[index]); });

            GameObject item = Instantiate(itemButtonPrefab, listItemsUnit.transform);   //��������� itemButtonPrefab � listItems
            listObject.Add(item);    //���������� ������ item � ���� � ����������� ���������� ���������

            item.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = nameObject[index];
            item.name = "item " + (index + 1);
            item.transform.localPosition = new Vector3(0, 280 - index * 140, 0);
            item.GetComponent<Button>().onClick.AddListener(setItem);   //���������� ���������� Button �� ��������� setItem
        }
    }
    private void OnDisable()
    {
        MMStartMenu.DestroyList(listObject);
    }
    
    //������� � ���������� �������
    public void BackCreateBuildingArmy()
    {
        createBuildingArmy.SetActive(true);
        selectionSquad.SetActive(false);
    }
    
    public void BackMenuPatterns()
    {
        menuPatterns.SetActive(true);
        selectionSquad.SetActive(false);
    }

    public void ToEditSquad()
    {
        if (ID != -1)
        {
            selectionSquad.SetActive(false);
            editSquad.SetActive(true);
            squadName2.GetComponent<TextMeshProUGUI>().text = nameObject[ID - 1];
        }
    }
    
    public void setSelection(int itemID, string name)   //��������� �������� ������ �������
    {
        squadName.GetComponent<TextMeshProUGUI>().text = name;
        ID = itemID;
    }
}
