using UnityEngine;

public class MenuPatterns : MonoBehaviour
{
    [Header("�������� �����")]
    public GameObject startMenu;
    public GameObject menuPatterns;
    public GameObject selectionPatternArmy;
    public GameObject selectionGeneral;
    public GameObject selectionOfficer;
    public GameObject selectionSquad;
    public GameObject selectionUnit;
    public void BackToStartMenu()
    {
        startMenu.SetActive(true);
        menuPatterns.SetActive(false);
    }
    public void MenuPatternsToSelectionPatternArmy()
    {
        menuPatterns.SetActive(false);
        selectionPatternArmy.SetActive(true);
    }
    public void MenuPatternsToSelectionGeneral()
    {
        menuPatterns.SetActive(false);
        selectionGeneral.SetActive(true);
    }
    public void MenuPatternsToSelectionOfficer()
    {
        menuPatterns.SetActive(false);
        selectionOfficer.SetActive(true);
    }
    public void MenuPatternsToSelectionSquad()
    {
        menuPatterns.SetActive(false);
        selectionSquad.SetActive(true);
    }
    public void MenuPatternsToSelectionUnit()
    {
        menuPatterns.SetActive(false);
        selectionUnit.SetActive(true);
    }

}
