using DataClass;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SelectionPatternArmy : MonoBehaviour
{
    private static int idArmy = 0;
    public static int IdArmy { get => idArmy;}
    List<int> IDobject = new List<int>() { 0, 1, 2 };    //���� �������� �����, ��������� ������
    List<string> nameObject = new List<string>() { "����� ������", "����� ����������", "����� ������ �������" };
    public List<GameObject> listObject = new();
    [Header("�������� �����")]
    public GameObject menuPatterns;
    public GameObject selectionPatternArmy;
    public GameObject createPatternArmy;
    [Header("�������")]
    public GameObject itemButtonPrefab;
    [Header("�������� �������� ����")]
    public GameObject listItemsPatternArmy; //����, � ������� ����� ����������� ����������� ������ �����
    public GameObject nameArmy; //�������� �����

    public void OnEnable()
    {
        for (int i = 0; i < IDobject.Count; i++)  //����������� �������� ���������
        {
            int index = i;  //������� ��� ������ �����
            //�������� �������� ��� ������ �������
            UnityAction setItem = new UnityAction(() => {setSelection(index); });

            GameObject item = Instantiate(itemButtonPrefab, listItemsPatternArmy.transform);   //��������� itemButtonPrefab � listItems
            listObject.Add(item);    //���������� ������ item � ���� � ����������� ���������� ���������

            item.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = nameObject[index];
            item.name = "item " + (index + 1);
            item.transform.localPosition = new Vector3(0, 280 - index * 140, 0);
            item.GetComponent<Button>().onClick.AddListener(setItem);   //���������� ���������� Button �� ��������� setItem
        }
        StartCoroutine(setInitialSelection(IdArmy<0?0:IdArmy));
    }

    private IEnumerator setInitialSelection(int index)
    {
        yield return null;
        setSelection(index);
    }

    private void OnDisable()
    {
        MMStartMenu.DestroyList(listObject);
    }

    public void BackMenuPatterns()
    {
        menuPatterns.SetActive(true);
        selectionPatternArmy.SetActive(false);
    }

    public void SelectionPatternArmyToCreatePatternArmy()
    {
        selectionPatternArmy.SetActive(false);
        createPatternArmy.SetActive(true);
    }
    public void SelectionPatternArmyToCreateNewPatternArmy()
    {
        idArmy = -1;
        selectionPatternArmy.SetActive(false);
        createPatternArmy.SetActive(true);
    }



    public void setSelection(int index)   //��������� �������� ������ �������
    {
        int itemID = IDobject[index];
        string name = nameObject[index];
        nameArmy.GetComponent<TextMeshProUGUI>().text = name;
        idArmy = itemID;
    }
}
