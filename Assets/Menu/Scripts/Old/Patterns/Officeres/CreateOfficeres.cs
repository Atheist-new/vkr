using DataClass;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CreateOfficere : MonoBehaviour
{
    private static int idToCreateOfficere = 0;
    public static int IdToCreateOfficere { set => idToCreateOfficere = value; }
    private static int IDOfficere;
    List<string> nameObject = new List<string>() { "Офицер егерей", "Офицер скитальцев", "Офицер лесных стражей" };
    [Header("Экранные формы")]
    public GameObject selectionOfficere;
    public GameObject selectionItem;
    public GameObject createPatternArmy;
    public GameObject selectMinGeneral;
    public GameObject createOfficere;
    // Start is called before the first frame update
    public GameObject nameArmy; //Название армии
    void OnEnable()
    {
        // IDOfficere = SelectionOfficer.ID_Officere;
        nameArmy.GetComponent<TextMeshProUGUI>().text = nameObject[IDOfficere];
    }

    public void CreateGeneralToSelectionItem()
    {
        selectionItem.SetActive(true);
    }

    public void Back()
    {
        //if (idToCreateOfficere == 1)
        selectionOfficere.SetActive(true);
        //else if (idToCreateOfficere == 2)
        createOfficere.SetActive(false);
    }
    public void SaveGeneral()
    {
        if (idToCreateOfficere == 1)
        {
            selectionOfficere.SetActive(true);
        }
        else if (idToCreateOfficere == 2)
        {
            createPatternArmy.SetActive(true);
        }
        createOfficere.SetActive(false);
    }
}
