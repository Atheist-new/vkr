using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SelectionOfficer : MonoBehaviour
{
    public static int type = -1;
    int ID;
    List<int> IDobject = new List<int>() { 1, 2, 3 };    //���� �������� �����, ��������� ������
    List<string> nameObject = new List<string>() { "������� �����", "������� ����", "������� �����" };
    public List<GameObject> listObject = new();
    [Header("�������� �����")]
    public GameObject selectionOfficere;
    public GameObject editOfficere;
    public GameObject menuPatterns;
    [Header("�������")]
    public GameObject itemButtonPrefab;
    [Header("�������� �������� ����")]
    public GameObject generalArmy; //��� ��������
    public GameObject generalArmy2; //��� ��������
    public GameObject listItemsCharacter;   //����, � ������� ����� ����������� ����������� ������ ����������
    public GameObject skill5;
    public GameObject skill6;
    void OnEnable()
    {
        if(type == 0)
        {
            for (int i = 0; i < IDobject.Count; i++)  //����������� �������� ���������
            {
                int index = i;  //������� ��� ������ �����
                //�������� �������� ��� ������ �������
                UnityAction setItem = new UnityAction(() => { setSelection(IDobject[index], nameObject[index]); });

                GameObject item = Instantiate(itemButtonPrefab, listItemsCharacter.transform);   //��������� itemButtonPrefab � listItems
                listObject.Add(item);    //���������� ������ item � ���� � ����������� ���������� ���������

                item.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = nameObject[index];
                item.name = "item " + (index + 1);
                item.transform.localPosition = new Vector3(0, 280 - index * 140, 0);
                item.GetComponent<Button>().onClick.AddListener(setItem);   //���������� ���������� Button �� ��������� setItem
            }
        }
    }
    private void OnDisable()
    {
        MMStartMenu.DestroyList(listObject);
    }

    public void BackMenuPatterns()
    {
        selectionOfficere.SetActive(false);
        menuPatterns.SetActive(true);
    }

    public void ToEditOfficere()
    {
        selectionOfficere.SetActive(false);
        editOfficere.SetActive(true);
        //generalArmy2.GetComponent<TextMeshProUGUI>().text = nameObject[ID - 1];
    }


    public void setSelection(int itemID, string name)   //��������� �������� ������ �������
    {
        generalArmy.GetComponent<TextMeshProUGUI>().text = name;
        ID = itemID;
    }
}
