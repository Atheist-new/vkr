using DataClass;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class CreatePatternArmy : MonoBehaviour
{
    private static int idArmy;
    List<string> nameObject = new List<string>() { "����� ������", "����� ����������", "����� ������ �������" };
    [Header("�������� �����")]
    public GameObject selectionPatternArmy;
    public GameObject createPatternArmy;
    public GameObject selectionGeneral;
    public GameObject createBuildingArmy;
    // Start is called before the first frame update
    public GameObject nameArmy; //�������� �����
    void OnEnable()
    {
        idArmy = SelectionPatternArmy.IdArmy;
        if (idArmy >= 0)
            nameArmy.GetComponent<TextMeshProUGUI>().text = nameObject[idArmy];
        else nameArmy.GetComponent<TextMeshProUGUI>().text = "����� �����";
    }

    public void CreatePatternArmyToSelectionGeneral()
    {
        SelectCharacterMin.type = 0;
        selectionGeneral.SetActive(true);
    }

    public void BackSelectionPatternArmy()
    {
        selectionPatternArmy.SetActive(true);
        createPatternArmy.SetActive(false);
    }
    public void SavePatternArmy()
    {
        selectionPatternArmy.SetActive(true);
        createPatternArmy.SetActive(false);
    }
    public void ToCreateBuildingArmy()
    {
        createPatternArmy.SetActive(false);
        createBuildingArmy.SetActive(true);
    }

}
