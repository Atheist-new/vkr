using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class MMStartMenu : MonoBehaviour
{
    [Header("�������� �����")]
    public GameObject startMenu;
    public GameObject menuPatterns;
    public GameObject storyline;

    public void Start()
    {
        startMenu.SetActive(true);
    }
    public void ToMenuPatterns()
    {
        startMenu.SetActive(false);
        menuPatterns.SetActive(true);
    }
    public void ToStoryline()
    {
        startMenu.SetActive(false);
        storyline.SetActive(true);
    }

    public static void DestroyList(List<GameObject> listObject)
    {
        listObject.ForEach(listObject => { Destroy(listObject); });      //������������ �������� ���� ����������� ����� ��������
        listObject.Clear();
    }
}
