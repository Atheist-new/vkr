using UnityEngine;

namespace Menu.Scripts.Pages
{
    public class PageList: MonoBehaviour
    {
        [Header("Main Menu")]
        public Page StartMenu;
        public Page PatternMenu;
        public Page StorylineMenu;
        
        [Header("Patterns Army Menu")]
        public Page ArmyMenu;
        public Page GeneralsMenu;
        public Page OfficersMenu;
        public Page SoldiersMenu;
        public Page SquadsMenu;
       
        [Header("Units")]
        public Page CreateSoldierMenu;
        
        [Header("Officers")]
        public Page CreateOfficerMenu;
        
        [Header("General")]
        public Page CreateGeneralMenu;

        [Header("Squads")] 
        public Page CreateSquadsMenu;

        [Header("Army")]
        public Page SelectArmyMenu;
        public Page CreateArmyMenu;
        public Page FormationArmyMenu;
    }
}