using System;
using Menu.Scripts.Routing;
using UnityEngine;

namespace Menu.Scripts.Pages.Implements
{
    public class SelectArmy : Page
    {
        public GameObject createNewArmy;
        public GameObject editArmy;
        
        private void Start()
        {
            OnClick("TopBrowser/Back", Router.Main.Back);
            
            OnClick(createNewArmy, OnCreateArmy);
            OnClick(editArmy, OnEditArmy);
        }

        private void OnCreateArmy()
        {
            Router.Main.To("CreateArmyMenu");
        }

        private void OnEditArmy()
        {
            Router.Main.To("CreateArmyMenu");
        }
    }
}