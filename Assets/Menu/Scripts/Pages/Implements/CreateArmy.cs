using System;
using Menu.Scripts.Repositories;
using Menu.Scripts.Routing;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Menu.Scripts.Pages.Implements
{
    public class CreateArmy : Page
    {
        public GameObject editGeneral;
        public GameObject selectMinGeneral;
        public GameObject generalViewPort;
        
        public GameObject characteristicsButton;
        public GameObject abilityBoard;
        public GameObject characteristicsWindow;
        public GameObject buttonSquadFront;
        
        private GameObject _characteristics;
        private GameObject _generalNameInViewport;

        private GameObject _squadButton1;
        private GameObject _squadButton2;
        private GameObject _squadButton3;
        private GameObject _squadButton4;
        private GameObject _squadButton5;
        
        private void Awake()
        {
            _generalNameInViewport = generalViewPort.transform.Find("TopPanel/CharacterName").gameObject;

            _characteristics = characteristicsWindow.transform.Find("Characteristics").gameObject;

            _squadButton1 = buttonSquadFront.transform.Find("Squad1").gameObject;
            _squadButton2 = buttonSquadFront.transform.Find("Squad2").gameObject;
            _squadButton3 = buttonSquadFront.transform.Find("Squad3").gameObject;
            _squadButton4 = buttonSquadFront.transform.Find("Squad4").gameObject;
            _squadButton5 = buttonSquadFront.transform.Find("Squad5").gameObject;
        }

        private void Start()
        {
            OnClick("TopBrowser/Back", Router.Main.Back);
            
            OnClick(editGeneral, OnGeneralMinMenu);
            
            OnClick(characteristicsButton, ShowCharacteristics);
            
            OnClick(_squadButton1, OnFormationArmyMenu);
            OnClick(_squadButton2, OnFormationArmyMenu);
            OnClick(_squadButton3, OnFormationArmyMenu);
            OnClick(_squadButton4, OnFormationArmyMenu);
            OnClick(_squadButton5, OnFormationArmyMenu);
        }

        private void OnGeneralMinMenu()
        {
            Debug.Log("1");
            selectMinGeneral.SetActive(true);
        }

        private void OnFormationArmyMenu()
        {
            Router.Main.To("FormationArmyMenu");
        }

        public void CharacterSelection(int generalID)
        {
            var generalData = Repository.Instance.FindGeneral(generalID);

            _generalNameInViewport.GetComponent<TextMeshProUGUI>().text = generalData.Name;
            
            _characteristics.transform.Find("PhysicalDamage/PhysicalDamage_Indicator").GetComponent<TextMeshProUGUI>().text = generalData.PhysicalDamage.ToString();
            _characteristics.transform.Find("MagicDamage/MagicDamage_Indicator").GetComponent<TextMeshProUGUI>().text = generalData.MagicDamage.ToString();
            _characteristics.transform.Find("AttackSpeed/AttackSpeed_Indicator").GetComponent<TextMeshProUGUI>().text = generalData.AttackSpeed.ToString();
            _characteristics.transform.Find("AttackRange/AttackRange_Indicator").GetComponent<TextMeshProUGUI>().text = generalData.AttackRange.ToString();
            _characteristics.transform.Find("Health/Health_Indicator").GetComponent<TextMeshProUGUI>().text = generalData.Health.ToString();
            _characteristics.transform.Find("Protection/Protection_Indicator").GetComponent<TextMeshProUGUI>().text = generalData.Protection.ToString();
            _characteristics.transform.Find("MagicalProtection/MagicalProtection_Indicator").GetComponent<TextMeshProUGUI>().text = generalData.MagicalProtection.ToString();
            _characteristics.transform.Find("MoveSpeed/MoveSpeed_Indicator").GetComponent<TextMeshProUGUI>().text = generalData.MoveSpeed.ToString();
            _characteristics.transform.Find("LoadCapacity/LoadCapacity_Indicator").GetComponent<TextMeshProUGUI>().text = generalData.LoadCapacity.ToString();
        }

        private void ShowCharacteristics()
        {
            characteristicsButton.GetComponent<Button>().onClick.RemoveAllListeners();
            
            abilityBoard.SetActive(false);
            characteristicsWindow.SetActive(true);
            
            OnClick(characteristicsButton, ShowAbility);
        }

        private void ShowAbility()
        {
            characteristicsButton.GetComponent<Button>().onClick.RemoveAllListeners();
            
            characteristicsWindow.SetActive(false);
            abilityBoard.SetActive(true);
            
            OnClick(characteristicsButton, ShowCharacteristics);
        }
        
    }
}