using System.Collections.Generic;
using Menu.Scripts.Routing;
using TMPro;
using Types.DTOs;
using UnityEngine;

namespace Menu.Scripts.Pages.Implements
{
    public class StorylinePage : Page
    {
        public GameObject storylinePrefab; //Префаб для создания сюжетных линий
        public Transform buttons;
        private void Start()
        {
            OnClick("Back", Router.Main.Back);
            
            var lines = new List<StorylineDto>{
                new (1, "Цена величия"),
                new (2, "Вторжение"),
                new (3, "Оборона"),
            };
            
            GenerateStoryline(lines);
        }
        //Создание префаба в зависимости от передачи данных
        private void GenerateStoryline(List<StorylineDto> lines)
        {
            for (var index = 0; index < lines.Count; index++)
            {
                var storyline = lines[index];
                var card = Instantiate(storylinePrefab, buttons);
                card.transform.localPosition = new Vector2((index - 1) * 600, 0);
                card.transform.Find("NameStoryline").GetComponent<TextMeshProUGUI>().text = storyline.Name;
                Debug.Log(storyline);
            }
        }
    }
}
