using System;
using System.Collections.Generic;
using System.Globalization;
using Extensions;
using Menu.Scripts.Repositories;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Menu.Scripts.Pages.Implements
{
    public class UnitsMinMenu : Page
    {
        public GameObject squadsCardPrefab;
        public GameObject creationPosition;
        public List<GameObject> _listSquads = new();
        public GameObject characteristicsWindow;
        public GameObject characterViewport;
        public GameObject squadName;
        public GameObject saveButton;
        public FormationArmy formationArmy;

        private GameObject _characteristics;
        private GameObject _characterNameInViewport;

        private GameObject _currentSize;
        private GameObject _maxSize;

        private int _globalSquadID;

        private void Awake()
        {
            _characteristics = characteristicsWindow.transform.Find("Characteristics").gameObject;

            _characterNameInViewport = characterViewport.transform.Find("TopPanel/CharacterName").gameObject;

            _currentSize = characterViewport.transform.Find("LowerPanel/CurrentSize").gameObject;
            
            _maxSize = characterViewport.transform.Find("LowerPanel/MaxSize").gameObject;
        }


        private void Start()
        {
            OnClick("EquipmentSelectionPanel/TopPanel/Back", () => gameObject.SetActive(false));
        }

        private void OnEnable()
        {
            GenerateSquad();
            OnClick(saveButton, SaveSquad);
        }

        private void OnDisable()
        {
            Repository.Instance.DestroyList(_listSquads);
        }

        private void GenerateSquad()
        {
            foreach (var (index, squadData) in Repository.Instance.SquadsData.Indexed())
            {
                // Генерация карточки, нахождение кнопки удаления и занесение в список карточки отряда
                var card = Instantiate(squadsCardPrefab, creationPosition.transform);
                _listSquads.Add(card);
                
                var deleteButton = card.transform.Find("DeleteButton").gameObject;

                // Расположение и название карточки отряда
                card.transform.localPosition = new Vector2(0, 305 - index * 140);
                card.transform.Find("Name").GetComponent<TextMeshProUGUI>().text = squadData.NameSquad;
                
                OnClick(card, () => SelectingSpecificSquad(squadData.IdSquad));
                
                OnClick(card, () => _globalSquadID = squadData.IdSquad);
            }
        }

        private void SelectingSpecificSquad(int squadID)
        {
            var squadData = Repository.Instance.FindSquads(squadID);
            var officerData = Repository.Instance.FindOfficer(squadData.IdOfficer);
            
            Repository.Instance.SelectSquad(_characteristics, officerData);
            _characterNameInViewport.GetComponent<TextMeshProUGUI>().text = officerData.Name;
            squadName.GetComponent<TextMeshProUGUI>().text = squadData.NameSquad;

            _currentSize.GetComponent<TextMeshProUGUI>().text = squadData.CurrentSquadSize.ToString(CultureInfo.InvariantCulture);
            _maxSize.GetComponent<TextMeshProUGUI>().text = officerData.SquadSize.ToString();
        }

        private void SaveSquad()
        {
            saveButton.GetComponent<Button>().onClick.RemoveAllListeners();
            
            formationArmy.SelectSquadOnFlang(_globalSquadID);

            gameObject.SetActive(false);
        }
    }
}