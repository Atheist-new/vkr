using System;
using System.Globalization;
using Menu.Scripts.Repositories;
using Menu.Scripts.Routing;
using TMPro;
using Types.DTOs;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

namespace Menu.Scripts.Pages.Implements
{
    public class CreateSquad : Page
    {
        [Header("Описание")] 
        public GameObject characterDescription;
        public GameObject characteristicsWindow;
        public GameObject magicPanel;
        public GameObject magicSkillList;
        [Header("Солдат")]
        public GameObject soldierName;
        public GameObject selectionSoldierButton;
        public GameObject selectionSoldierWindow;
        public GameObject backButtonSoldier;
        public GameObject soldierLowerPanel;
        [Header("Офицер")]
        public GameObject officerName;
        public GameObject selectionOfficerButton;
        public GameObject selectionOfficerWindow;
        public GameObject backButtonOfficer;
        public GameObject officerLowerPanel;
        [Header("Сохранение")] 
        public GameObject saveButton;
        public GameObject squadName;
        [Header("Характеристики")] 
        public GameObject characteristics;
        public GameObject squadSizeIndicator;
        public GameObject squadSizeLine;
        private int _startID;
        [Header("Ссылки")]
        public SquadsMenu squadsMenu;
        [Header("Слайдер")] 
        public GameObject slider;
        public GameObject textEditingButton;

        private int _soldierID;
        private int _officerID;
        private float _currentSquadSize;

        public GameObject backButton;
            
        private void Start()
        {
            // Кнопки перемещения
            OnClick(selectionSoldierButton, OnSoldierMinMenu);
            OnClick(selectionOfficerButton, OnOfficerMinMenu);

            backButton = transform.Find("TopBrowser/Back").gameObject;
            
            // Слайдер
            slider.transform.Find("SliderBar").GetComponent<Slider>().onValueChanged.AddListener(OnSliderValueChanged);
        }

        private void OnEnable()
        {
            textEditingButton.GetComponent<Button>().onClick.RemoveAllListeners();
            OnClick(textEditingButton, () => squadName.GetComponent<TMP_InputField>().interactable = true);
        }

        private void OnDisable()
        {
            squadName.GetComponent<TMP_InputField>().interactable = false;
        }

        private void OnMenuFromCreate()
        {
            squadsMenu.LastSelectedSquad();
            Debug.Log("Переход из создания отряда");
            Router.Main.Back();
        }
        
        public void CreateNewSquad()
        {
            saveButton.GetComponent<Button>().onClick.RemoveAllListeners();
            backButton.GetComponent<Button>().onClick.RemoveAllListeners();
            
            OnClick(saveButton, SaveNewSquad);
            OnClick(backButton, OnMenuFromCreate);

            _officerID = 0;
            _soldierID = 0;
            _currentSquadSize = 0;
            
            soldierLowerPanel.SetActive(false);
            officerLowerPanel.SetActive(false);
            characteristicsWindow.SetActive(false);
            magicPanel.SetActive(false);
            magicSkillList.SetActive(false);
            
            squadName.GetComponent<TMP_InputField>().text = "";
            Repository.Instance.UnselectedCharacter(characteristics, magicPanel);
            soldierName.GetComponent<TextMeshProUGUI>().text = "Выберите солдата";
            officerName.GetComponent<TextMeshProUGUI>().text = "Выберите офицера";
            squadSizeIndicator.SetActive(false);

            // Параметры слайдера
            slider.transform.Find("SliderBar").GetComponent<Slider>().interactable = false;
            slider.transform.Find("SliderBar").GetComponent<Slider>().value = 0;
            slider.transform.Find("CurrentValue").GetComponent<TextMeshProUGUI>().text = "-";
            slider.transform.Find("MaxValue").GetComponent<TextMeshProUGUI>().text = "-";
        }
        
        // Редактирование оряда
        public void EditSquad(int officerID, int soldierID, int squadID)
        {
            saveButton.GetComponent<Button>().onClick.RemoveAllListeners();
            backButton.GetComponent<Button>().onClick.RemoveAllListeners();
            
            OnClick(saveButton,() => SaveEditingSquad(squadID));
            OnClick(backButton, Router.Main.Back);
            OnClick(backButton, () => Debug.Log("Переход из редактирования отряда"));
            
            var squadData = Repository.Instance.FindSquads(squadID);
            var officerData = Repository.Instance.FindOfficer(officerID);
            var soldierData = Repository.Instance.FindSoldier(soldierID);
            
            soldierLowerPanel.SetActive(true);
            officerLowerPanel.SetActive(true);
            characteristicsWindow.SetActive(true);
            magicPanel.SetActive(true);
            magicSkillList.SetActive(true);
            
            _officerID = squadData.IdOfficer;
            _soldierID = squadData.IdSoldier;
            _currentSquadSize = squadData.CurrentSquadSize;

            squadName.GetComponent<TMP_InputField>().text = squadData.NameSquad;
            soldierName.GetComponent<TextMeshProUGUI>().text = soldierData.Name;
            officerName.GetComponent<TextMeshProUGUI>().text = officerData.Name;
            
            // Характеристики персонажа
            magicPanel.transform.Find("Skill1").GetComponent<Image>().color = soldierData.MagicSkill1;
            magicPanel.transform.Find("Skill2").GetComponent<Image>().color = soldierData.MagicSkill2;
            squadSizeIndicator.GetComponent<TextMeshProUGUI>().text = officerData.SquadSize.ToString();
            Repository.Instance.SelectSquad(characteristics, officerData);
            
            //
            slider.transform.Find("MaxValue").GetComponent<TextMeshProUGUI>().text = officerData.SquadSize.ToString();
            slider.transform.Find("SliderBar").GetComponent<Slider>().maxValue = officerData.SquadSize;
            slider.transform.Find("CurrentValue").GetComponent<TextMeshProUGUI>().text = squadData.CurrentSquadSize.ToString(CultureInfo.InvariantCulture);
            slider.transform.Find("SliderBar").GetComponent<Slider>().value = squadData.CurrentSquadSize;
        }
        
         // Сохранение нового отряда
        private void SaveNewSquad()
        {
            if (_officerID != 0 && _soldierID != 0 && _currentSquadSize != 0)
            {
                // Вносим новый отряд в базу данных
                Repository.Instance.SquadsData.Add(new SquadsDto(++_startID,
                    squadName.GetComponent<TMP_InputField>().text, _soldierID, _officerID, _currentSquadSize));

                squadsMenu.NewSaveSquad(_startID);
                Router.Main.Back();
            }
            else if (_officerID == 0 || _soldierID == 0 || _currentSquadSize == 0)
            {
                Debug.Log("Данные некоректны, проверьте состояние отряда");
            }
        }
        
        // Сохранение отредактированного отряда
        private void SaveEditingSquad(int squadID)
        {
            if (_currentSquadSize == 0) return;
            
            var squadData = Repository.Instance.FindSquads(squadID);
            
            // Изменение данных в базе
            squadData.NameSquad = squadName.GetComponent<TMP_InputField>().text;
            squadData.IdSoldier = _soldierID;
            squadData.IdOfficer = _officerID;
            squadData.CurrentSquadSize = _currentSquadSize;

            squadsMenu.SelectingSpecificSquad(squadID);

            Router.Main.Back();
        }
        
        // Изменение значения слайдера
        private void OnSliderValueChanged(float value)
        {
            _currentSquadSize = slider.transform.Find("SliderBar").GetComponent<Slider>().value;
            slider.transform.Find("CurrentValue").GetComponent<TextMeshProUGUI>().text = _currentSquadSize.ToString(CultureInfo.InvariantCulture);
        }
        
        // Переход во всплывающие окно выбора солдата
        private void OnSoldierMinMenu()
        {
            selectionSoldierWindow.SetActive(true);
            OnClick(backButtonSoldier, CloseSelectionUnit);
        }
        
        // Переход во всплывающие окно выбора офицера
        private void OnOfficerMinMenu()
        {
            selectionOfficerWindow.SetActive(true);
            OnClick(backButtonOfficer, CloseSelectionUnit);
        }
        
        // Закрытие окна выбора персонажа
        public void CloseSelectionUnit()
        {
            selectionSoldierWindow.SetActive(false);
            selectionOfficerWindow.SetActive(false);
        }
        
        // Отображение данных о выбранном солдате
        public void SoldierSelection(int dtoCharacterID)
        {
            var soldierDto = Repository.Instance.FindSoldier(dtoCharacterID);
            soldierName.GetComponent<TextMeshProUGUI>().text = soldierDto.Name;
            
            soldierLowerPanel.SetActive(true);
            // Характеристики персонажа
            magicPanel.transform.Find("Skill1").GetComponent<Image>().color = soldierDto.MagicSkill1;
            magicPanel.transform.Find("Skill2").GetComponent<Image>().color = soldierDto.MagicSkill2;
            
            _soldierID = soldierDto.ID;
            
            SelectingSpecificSquad();
        }
        
        // Отображение данных о выбранном офицере
        public void OfficerSelection(int dtoCharacterID)
        {
            var officerDto = Repository.Instance.FindOfficer(dtoCharacterID);
            officerName.GetComponent<TextMeshProUGUI>().text = officerDto.Name;
            
            _officerID = officerDto.ID;
            
            officerLowerPanel.SetActive(true);
            squadSizeLine.GetComponent<TextMeshProUGUI>().text = officerDto.SquadSize.ToString();
            slider.transform.Find("SliderBar").GetComponent<Slider>().interactable = true;
            slider.transform.Find("SliderBar").GetComponent<Slider>().maxValue = officerDto.SquadSize;
            squadSizeIndicator.GetComponent<TextMeshProUGUI>().text = officerDto.SquadSize.ToString();

            SelectingSpecificSquad();
        }

        // Отображения данных о выбранном отряде
        private void SelectingSpecificSquad()
        {
            var officerDto = Repository.Instance.FindOfficer(_officerID);

            if (_officerID == 0 || _soldierID == 0) return;
            
            characteristicsWindow.SetActive(true);
            magicPanel.SetActive(true);
            magicSkillList.SetActive(true);
            squadSizeIndicator.SetActive(true);
            
            characteristics.transform.Find("PhysicalDamage/PhysicalDamage_Indicator").GetComponent<TextMeshProUGUI>().text = (officerDto.PhysicalDamage * 3).ToString();
            characteristics.transform.Find("MagicDamage/MagicDamage_Indicator").GetComponent<TextMeshProUGUI>().text = (officerDto.MagicDamage * 3).ToString();
            characteristics.transform.Find("AttackSpeed/AttackSpeed_Indicator").GetComponent<TextMeshProUGUI>().text = (officerDto.AttackSpeed * 3).ToString();
            characteristics.transform.Find("AttackRange/AttackRange_Indicator").GetComponent<TextMeshProUGUI>().text = (officerDto.AttackRange * 3).ToString();
            characteristics.transform.Find("Health/Health_Indicator").GetComponent<TextMeshProUGUI>().text = (officerDto.Health * 3).ToString();
            characteristics.transform.Find("Protection/Protection_Indicator").GetComponent<TextMeshProUGUI>().text = (officerDto.Protection * 3).ToString();
            characteristics.transform.Find("MagicalProtection/MagicalProtection_Indicator").GetComponent<TextMeshProUGUI>().text = (officerDto.MagicalProtection * 3).ToString();
            characteristics.transform.Find("MoveSpeed/MoveSpeed_Indicator").GetComponent<TextMeshProUGUI>().text = (officerDto.MoveSpeed * 3).ToString();
            characteristics.transform.Find("LoadCapacity/LoadCapacity_Indicator").GetComponent<TextMeshProUGUI>().text = (officerDto.LoadCapacity * 3).ToString();
        }
    }
}
