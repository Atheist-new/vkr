using Menu.Scripts.Repositories;
using Menu.Scripts.Routing;
using TMPro;
using UnityEngine;

namespace Menu.Scripts.Pages.Implements
{
    public class CreateGeneral : Page
    {
        [Header("��������")]
        public GameObject characterName;
        
        [Header("��������������")] 
        public GameObject characterPhysicalDamage;
        public GameObject characterMagicDamage;
        public GameObject characterAttackSpeed;
        public GameObject characterAttackRange;
        public GameObject characterHeath;
        public GameObject characterProtection;
        public GameObject characterMagicalProtection;
        public GameObject characterMoveSpeed;
        public GameObject characterLoadCapacity;

        private void Start()
        {
            OnClick("TopBrowser/Back", Router.Main.Back);
        }

        public void CharacterSelection(int dtoCharacterID)
        {
            var dto = Repository.Instance.FindGeneral(dtoCharacterID);
            SelectedCharacterName(dto.Name);
            
            characterPhysicalDamage.GetComponent<TextMeshProUGUI>().text = dto.PhysicalDamage.ToString();
            characterMagicDamage.GetComponent<TextMeshProUGUI>().text = dto.MagicDamage.ToString();
            characterAttackSpeed.GetComponent<TextMeshProUGUI>().text = dto.AttackSpeed.ToString();
            characterAttackRange.GetComponent<TextMeshProUGUI>().text = dto.AttackRange.ToString();
            characterHeath.GetComponent<TextMeshProUGUI>().text = dto.Health.ToString();
            characterProtection.GetComponent<TextMeshProUGUI>().text = dto.Protection.ToString();
            characterMagicalProtection.GetComponent<TextMeshProUGUI>().text = dto.MagicalProtection.ToString();
            characterMoveSpeed.GetComponent<TextMeshProUGUI>().text = dto.MoveSpeed.ToString();
            characterLoadCapacity.GetComponent<TextMeshProUGUI>().text = dto.LoadCapacity.ToString();
        }
        //
        private void SelectedCharacterName(string dtoCharacterName)
        {
            characterName.GetComponent<TextMeshProUGUI>().text = dtoCharacterName;
        }
    }
}