using System.Collections.Generic;
using Extensions;
using Menu.Scripts.Repositories;
using Menu.Scripts.Routing;
using TMPro;
using UnityEngine;

namespace Menu.Scripts.Pages.Implements
{
    public class OfficersMinMenu : Page
    {
        [Header("Меню")] 
        public GameObject officerEditingButton;
        public GameObject saveUnitButton;
        
        [Header("Создание персонажей")]
        public GameObject creationPosition;
        public GameObject characterButtonPrefab;
        public List<GameObject> listCharacters = new();

        [Header("Характеристики")] 
        public GameObject characteristics;
        public GameObject squadSizeIndicator;
        private int _unitID;
        
        [Header("Окно просмотра персонажа")] 
        public GameObject characterViewport;
        
        [Header("Ссылки")]
        public CreateOfficer createOfficer;
        public CreateSquad createSquad;
        
        private void Start()
        {
            OnClick(officerEditingButton, OnOfficersMenu);
        }

        private void OnOfficersMenu()
        {
            Router.Main.To("CreateOfficerMenu");
        }
        
        private void OnEnable()
        {
            GenerateCharacters();
            OnClick(saveUnitButton, SaveOfficer);
        }
        //Создание персонажей
        private void GenerateCharacters()
        {
            foreach (var (index, unit) in Repository.Instance.OfficersData.Indexed())
            {
                var card = Instantiate(characterButtonPrefab, creationPosition.transform);
                listCharacters.Add(card);
                card.transform.localPosition = new Vector2(0,305 - index * 130);
                card.transform.Find("Name").GetComponent<TextMeshProUGUI>().text = unit.Name;
                //Отправляем данные о выбранном персонаже
                OnClick(card, () => CharacterSelection(unit.ID));
                //Отправляем данные о выбранном персонаже в окно редактирования
                OnClick(card, () => createOfficer.CharacterSelection(unit.ID));
                //Отправляем данные о выбранном персонаже в окно создания отряда
                OnClick(card, () => _unitID = unit.ID);
            }
        }

        private void SaveOfficer()
        {
            createSquad.OfficerSelection(_unitID);
            createSquad.CloseSelectionUnit();
        }
        
        //Отключение списка персонажей
        private void OnDisable()
        {
            MMStartMenu.DestroyList(listCharacters);
        }
        //Отображение выбранного персонажа
        private void CharacterSelection(int id)
        {
            var dto = Repository.Instance.FindOfficer(id);
            SelectedCharacterName(dto.Name);
            //Характеристики персонажа
            Repository.Instance.SelectOfficer(characteristics, dto, squadSizeIndicator);
        }
        //Отображение имени выбранного персонажа
        private void SelectedCharacterName(string characterName)
        {
            var characterNameInViewport = characterViewport.transform.Find("TopPanel/CharacterName");
            characterNameInViewport.GetComponent<TextMeshProUGUI>().text = characterName;
        }
    }
}