using System.Collections.Generic;
using Extensions;
using Menu.Scripts.Repositories;
using TMPro;
using UnityEngine;

namespace Menu.Scripts.Pages.Implements
{
    public class GeneralMinMenu : Page
    {
        public GameObject characterButtonPrefab;
        public GameObject creationPosition;
        public List<GameObject> listCharacters = new();

        public GameObject characteristicsWindow;
        private GameObject _characteristics;

        public GameObject characterViewPort;
        private GameObject _characterNameInViewPort;

        public GameObject saveButton;
        private GameObject _backButton;

        public CreateArmy createArmy;

        private int _globalGeneralID;

        private void Awake()
        {
            _characteristics = characteristicsWindow.transform.Find("Characteristics").gameObject;

            _characterNameInViewPort = characterViewPort.transform.Find("TopPanel/CharacterName").gameObject;

            _backButton = gameObject.transform.Find("EquipmentSelectionPanel/TopPanel/Back").gameObject;
        }

        private void Start()
        {
            OnClick(_backButton, () => gameObject.SetActive(false));
        }

        private void OnEnable()
        {
            GenerateCharacters();
            OnClick(saveButton, SaveGeneral);
        }

        private void OnDisable()
        {
            Repository.Instance.DestroyList(listCharacters);
        }

        private void GenerateCharacters()
        {
            foreach (var (index, unit) in Repository.Instance.GeneralsData.Indexed())
            {
                var card = Instantiate(characterButtonPrefab, creationPosition.transform);
                listCharacters.Add(card);
                card.transform.localPosition = new Vector2(0,305 - index * 130);
                card.transform.Find("Name").GetComponent<TextMeshProUGUI>().text = unit.Name;

                OnClick(card, () => CharacterSelection(unit.ID));
                OnClick(card, () => _globalGeneralID = unit.ID);
            }
        }

        private void CharacterSelection(int generalID)
        {
            var generalData = Repository.Instance.FindGeneral(generalID);
            
            _characteristics.transform.Find("PhysicalDamage/PhysicalDamage_Indicator").GetComponent<TextMeshProUGUI>().text = generalData.PhysicalDamage.ToString();
            _characteristics.transform.Find("MagicDamage/MagicDamage_Indicator").GetComponent<TextMeshProUGUI>().text = generalData.MagicDamage.ToString();
            _characteristics.transform.Find("AttackSpeed/AttackSpeed_Indicator").GetComponent<TextMeshProUGUI>().text = generalData.AttackSpeed.ToString();
            _characteristics.transform.Find("AttackRange/AttackRange_Indicator").GetComponent<TextMeshProUGUI>().text = generalData.AttackRange.ToString();
            _characteristics.transform.Find("Health/Health_Indicator").GetComponent<TextMeshProUGUI>().text = generalData.Health.ToString();
            _characteristics.transform.Find("Protection/Protection_Indicator").GetComponent<TextMeshProUGUI>().text = generalData.Protection.ToString();
            _characteristics.transform.Find("MagicalProtection/MagicalProtection_Indicator").GetComponent<TextMeshProUGUI>().text = generalData.MagicalProtection.ToString();
            _characteristics.transform.Find("MoveSpeed/MoveSpeed_Indicator").GetComponent<TextMeshProUGUI>().text = generalData.MoveSpeed.ToString();
            _characteristics.transform.Find("LoadCapacity/LoadCapacity_Indicator").GetComponent<TextMeshProUGUI>().text = generalData.LoadCapacity.ToString();

            _characterNameInViewPort.GetComponent<TextMeshProUGUI>().text = generalData.Name;
        }

        private void SaveGeneral()
        {
            gameObject.SetActive(false);
            createArmy.CharacterSelection(_globalGeneralID);
        }
    }
}