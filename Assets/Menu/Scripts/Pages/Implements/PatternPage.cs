using Menu.Scripts.Routing;

namespace Menu.Scripts.Pages.Implements
{
    public class PatternPage: Page
    {
        private void Start()
        {
            OnClick("TopBrowser/Back", Router.Main.Back);
            
            OnClick("ArmyPatterns", OnSelectionArmyMenu);
            OnClick("SoldierPatterns", OnSoldiersMenu);
            OnClick("SquadPatterns", OnSquadsMenu);
            OnClick("GeneralPatterns", OnGeneralsMenu);
            OnClick("OfficerPatterns", OnOfficersMenu);
        }
        
        private void OnSelectionArmyMenu()
        {
            Router.Main.To("SelectArmyMenu");
        }
        
        private void OnSoldiersMenu()
        {
            Router.Main.To("SoldiersMenu");
        }

        private void OnSquadsMenu()
        {
            Router.Main.To("SquadsMenu");
        }
        
        private void OnGeneralsMenu()
        {
            Router.Main.To("GeneralsMenu");
        }

        private void OnOfficersMenu()
        {
            Router.Main.To("OfficersMenu");
        }
    }
}