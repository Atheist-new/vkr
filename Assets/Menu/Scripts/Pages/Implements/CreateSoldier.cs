using Menu.Scripts.Repositories;
using Menu.Scripts.Routing;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Menu.Scripts.Pages.Implements
{
    public class CreateSoldier : Page
    {
        [Header("��������")]
        public GameObject characterName;
        public GameObject magicPanel;
        
        [Header("��������")] 
        public GameObject characterDescription;

        [Header("��������������")] 
        public GameObject characteristics;

        private void Start()
        {
            OnClick("TopBrowser/Back", Router.Main.Back);
            OnClick("TopBrowser/Description", OpenDescription);
            OnClick("UnitDescription/Panel/Close", CloseDescription);
        }
        //
        private void OpenDescription()
        {
            characterDescription.SetActive(true);
        }
        //
        private void CloseDescription()
        {
            characterDescription.SetActive(false);
        }
        private void OnDisable()
        {
            CloseDescription();
        }

        public void CharacterSelection(int dtoCharacterID)
        {
            var dto = Repository.Instance.FindSoldier(dtoCharacterID);
            SelectedCharacterName(dto.Name);
            Repository.Instance.SelectSoldier(characteristics, dto, magicPanel);
        }
        //
        private void SelectedCharacterName(string dtoCharacterName)
        {
            characterName.GetComponent<TextMeshProUGUI>().text = dtoCharacterName;
        }
    }
}