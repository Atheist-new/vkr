using System.Collections.Generic;
using System.Globalization;
using Extensions;
using Menu.Scripts.Repositories;
using Menu.Scripts.Routing;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Menu.Scripts.Pages.Implements
{
    public class SquadsMenu : Page
    {
        [Header("Кнопки переходов")]
        public GameObject newSquadButton;
        public GameObject editSquadButton;
        [Header("Генерация отрядов")]
        public GameObject squadsCardPrefab;
        public GameObject creationPosition;
        [Header("Данные о отряде")]
        public GameObject characteristicWindow;
        public GameObject nameSquad;
        [Header("Переключение солдата/офицера")]
        public GameObject switchingButtons;
        [Header("Окно предпросмотра")]
        public GameObject squadViewport;
        [Header("Панели навыков")]
        public GameObject officerPanel;
        public GameObject soldierPanel;
        [Header("Окно подтверждения")]
        public GameObject darkBackground;
        public GameObject confirmDeleteWindow;
        [Header("Ссылки")]
        public CreateSquad createSquad;

        private List<GameObject> _listSquads = new(); // Список хранящих отряды
        private GameObject _characterctics; // Характеристики отряда
        private GameObject _characterName; // Имя отображаемого персонажа
        private GameObject _lowerPanel; // Нижняя панель окна предпросмотра
        
        // Способности солдата
        private GameObject _soldierSkill1;
        private GameObject _soldierSkill2;
        
        private int _globalSquadID; // Глобальный id отряда
        private int _lastSquadID;   // id последнего выбранного отряда

        private void Awake()
        {
            OnClick("TopBrowser/Back", Router.Main.Back);
            OnClick(newSquadButton.gameObject, OnCreateSquad);
            OnClick(editSquadButton.gameObject, OnEditSquad);
            
            // Переключение солдата/офицера в окне просмотра
            var officerButton = switchingButtons.transform.Find("OfficerButton").gameObject;
            var soldierButton = switchingButtons.transform.Find("SoldierButton").gameObject;
            OnClick(officerButton, OfficerViewport);
            OnClick(soldierButton, SoldierViewport);
            
            // Находим и сохраняем ссылки на панели навыков солдата
            _soldierSkill1 = soldierPanel.transform.Find("Skills/Skill_1").gameObject;
            _soldierSkill2 = soldierPanel.transform.Find("Skills/Skill_2").gameObject;

            // Находим нижнюю паннель и имя персонажа в окне предпросмотра
            _lowerPanel = squadViewport.transform.Find("LowerPanel").gameObject;
            _characterName = squadViewport.transform.Find("TopPanel/CharacterName").gameObject;
            
            // Находим характеристики в окне характеристик
            _characterctics = characteristicWindow.transform.Find("Characteristics").gameObject;

            EmptySquad();
        }

        private void OnEnable()
        {
            GenerateSquad();
        }
        
        private void OnDisable()
        {
            Repository.Instance.DestroyList(_listSquads);
        }

        // В меню создания отряда
        private void OnCreateSquad()
        {
            Router.Main.To("CreateSquadMenu");
            _lastSquadID = _globalSquadID;
            _globalSquadID = 0;
            createSquad.CreateNewSquad();
        }

        // Сохранение последнего выбранного отряда
        public void LastSelectedSquad()
        {
            _globalSquadID = _lastSquadID;
        }

        // Выбор нового сохраненного отряда как основного
        public void NewSaveSquad(int saveSquadID)
        {
            _globalSquadID = saveSquadID;
            SelectingSpecificSquad(_globalSquadID);
        }
        
        // В меню редактирование отряда
        private void OnEditSquad()
        {
            if (_globalSquadID == 0) return;
            
            var squadDto = Repository.Instance.FindSquads(_globalSquadID);
            createSquad.EditSquad(squadDto.IdOfficer, squadDto.IdSoldier, _globalSquadID);
            
            Router.Main.To("CreateSquadMenu");
        }
        
        // Создание карточек отряда
        private void GenerateSquad()
        {
            foreach (var (index, squadData) in Repository.Instance.SquadsData.Indexed())
            {
                // Генерация карточки, нахождение кнопки удаления и занесение в список карточки отряда
                var card = Instantiate(squadsCardPrefab, creationPosition.transform);
                _listSquads.Add(card);
                
                var deleteButton = card.transform.Find("DeleteButton").gameObject;

                // Расположение и название карточки отряда
                card.transform.localPosition = new Vector2(0, 310 - index * 130);
                card.transform.Find("Name").GetComponent<TextMeshProUGUI>().text = squadData.NameSquad;

                // ID с карточки
                OnClick(card, () => _globalSquadID = squadData.IdSquad);
                // Выбор определенного отряда при нажатии на карточку
                OnClick(card, () => SelectingSpecificSquad(squadData.IdSquad));
                // При нажатии по карточке в окне предпросмотра выбирается офицер
                OnClick(card, OfficerViewport);
                // Кнопка удаления отряда на карточке
                OnClick(deleteButton, () => ConfirmDelete(squadData.IdSquad, card));

                if (_globalSquadID == 0 && Repository.Instance.SquadsData != null)
                {
                    _globalSquadID = squadData.IdSquad;
                    SelectingSpecificSquad(squadData.IdSquad);
                }
            }
        }
        
        // Вызов пустого отряда
        private void EmptySquad()
        {
            editSquadButton.SetActive(false);
            // Изменяем названия
            _characterName.GetComponent<TextMeshProUGUI>().text = "Офицер не выбран";
            nameSquad.GetComponent<TextMeshProUGUI>().text = "Выберите отряд";
            
            // Отключаем панели
            characteristicWindow.SetActive(false);
            soldierPanel.SetActive(false);
            officerPanel.SetActive(false);
            _lowerPanel.SetActive(false);
            switchingButtons.SetActive(false);
        }
        
        // Выбор определенного отряда
        public void SelectingSpecificSquad(int squadID)
        {
            if (_globalSquadID == 0) return;
            
            editSquadButton.SetActive(true);
            
            // Находим индикаторы текущего и максимального размера отряда
            var squadCurrentSize = _lowerPanel.transform.Find("CurrentSize").gameObject;
            var squadMaxSize = _lowerPanel.transform.Find("MaxSize").gameObject;
            
            // Включение компонентов
            characteristicWindow.SetActive(true);
            switchingButtons.SetActive(true);
            
            var squadData = Repository.Instance.FindSquads(squadID);
            var officerData = Repository.Instance.FindOfficer(squadData.IdOfficer);

            OfficerViewport();
            // Отображение данных о выбраном отряде
            Repository.Instance.SelectSquad(_characterctics, officerData); //Подключение характеристик отряда
            squadMaxSize.GetComponent<TextMeshProUGUI>().text = officerData.SquadSize.ToString();
            squadCurrentSize.GetComponent<TextMeshProUGUI>().text = squadData.CurrentSquadSize.ToString(CultureInfo.InvariantCulture);
            nameSquad.GetComponent<TextMeshProUGUI>().text = squadData.NameSquad;
        }
        
        // Подтверждение удаления отряда
        private void ConfirmDelete(int squadID, GameObject card)
        {
            // Находим кнопки подтверждения/отмены в окне подтверждения
            var confirmDeleteButton = confirmDeleteWindow.transform.Find("DeleteButton").GetComponent<Button>();
            var cancellationButton = confirmDeleteWindow.transform.Find("BackButton").GetComponent<Button>();
            
            // Удаляем слущателей кнопок с предыдущей карточки отряда
            confirmDeleteButton.onClick.RemoveAllListeners();
            cancellationButton.onClick.RemoveAllListeners();
            
            // Выводим на экран окно подтверждения
            darkBackground.SetActive(true);
            confirmDeleteWindow.SetActive(true);
    
            // Добавляем слушателей на кнопки в окне подтверждения
            confirmDeleteButton.onClick.AddListener(() => DeleteSquad(squadID, card));
            cancellationButton.onClick.AddListener(CancellationDelete);
        }
        
        // Удаление отряда
        private void DeleteSquad(int squadID, GameObject card)
        {
            var squadData = Repository.Instance.FindSquads(squadID);

            if (squadData == null) return;
            
            // Удаление карточки отряда
            Repository.Instance.SquadsData.Remove(squadData);
            _listSquads.Remove(card);
            Destroy(card);
                
            // Закрываем окно подтверждения
            darkBackground.SetActive(false);
            confirmDeleteWindow.SetActive(false);
                
            // Обновляем список отрядов для коректного смещения карточек
            _globalSquadID = 0;
            Repository.Instance.DestroyList(_listSquads);
            EmptySquad();
            GenerateSquad();
        }
        
        // Отмена удаления отряда
        private void CancellationDelete()
        {
            // Закрывем окно подтверждения
            darkBackground.SetActive(false);
            confirmDeleteWindow.SetActive(false);
        }

        // Окно предпросмотра солдата
        private void SoldierViewport()
        {
            var squadData = Repository.Instance.FindSquads(_globalSquadID);
            var soldierData = Repository.Instance.FindSoldier(squadData.IdSoldier);

            _characterName.GetComponent<TextMeshProUGUI>().text = soldierData.Name;
            
            officerPanel.SetActive(false);
            soldierPanel.SetActive(true);

            // Изменение навыков солдата
            _soldierSkill1.GetComponent<Image>().color = soldierData.MagicSkill1;
            _soldierSkill2.GetComponent<Image>().color = soldierData.MagicSkill2;
        }
        
        // Окно предпросмотра офицера
        private void OfficerViewport()
        {
            var squadData = Repository.Instance.FindSquads(_globalSquadID);
            var officerData = Repository.Instance.FindOfficer(squadData.IdOfficer);

            _characterName.GetComponent<TextMeshProUGUI>().text = officerData.Name;

            _lowerPanel.SetActive(true);
            soldierPanel.SetActive(false);
            officerPanel.SetActive(true);
        }
    }
}
