using Menu.Scripts.Routing;
using UnityEngine;

namespace Menu.Scripts.Pages.Implements
{
    public class StartMenuPage: Page
    {
        public void Start()
        {
            OnClick("LeftButton/Shop", OnShop);
            OnClick("RightButton/Patterns", OnPatterns);
            OnClick("CenterButtons/Storyline", OnStoryline);
        }

        private void OnShop()
        {
            Debug.Log("We have drugs and we used them");
        }

        private void OnPatterns()
        {
            Router.Main.To("PatternMenu");
        }

        private void OnStoryline()
        {
            Router.Main.To("StorylineMenu");
        }
    }
}