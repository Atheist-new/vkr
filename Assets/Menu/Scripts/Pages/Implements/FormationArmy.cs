using System;
using Menu.Scripts.Repositories;
using Menu.Scripts.Routing;
using TMPro;
using UnityEngine;

namespace Menu.Scripts.Pages.Implements
{
    public class FormationArmy : Page
    {
        public GameObject button1;
        public GameObject button2;
        public GameObject unitsMinMenu;

        public int buttonCount;
        
        private void Start()
        {
            OnClick("TopBrowser/Back", Router.Main.Back);
            
            OnClick(button1, () => OnUnitMinMenu(1));
            OnClick(button2, () => OnUnitMinMenu(2));
        }

        private void OnUnitMinMenu(int count)
        {
            buttonCount = count;
            unitsMinMenu.SetActive(true);
        }

        public void SelectSquadOnFlang(int squadID)
        {
            var squadData = Repository.Instance.FindSquads(squadID);
            
            switch (buttonCount)
            {
                case 1:
                {
                    button1.transform.Find("NameS1").GetComponent<TextMeshProUGUI>().text = squadData.NameSquad; 
                    break;
                }
                case 2:
                {
                    button2.transform.Find("NameS2").GetComponent<TextMeshProUGUI>().text = squadData.NameSquad;
                    break;
                }
            }
        }
    }
}