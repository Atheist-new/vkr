using System;
using System.Collections.Generic;
using Extensions;
using Menu.Scripts.Repositories;
using Menu.Scripts.Routing;
using TMPro;
using UnityEngine;

namespace Menu.Scripts.Pages.Implements
{
    public class SoldiersMinMenu : Page
    {
        [Header("����")]
        public GameObject soldierEditingButton;
        public GameObject saveUnitButton;
        public GameObject magicPanel;
        
        [Header("�������� ����������")]
        public GameObject creationPosition;
        public GameObject characterButtonPrefab;
        public List<GameObject> listCharacters = new();
        
        [Header("��������������")] 
        public GameObject characteristics;
        private int _unitID;
        
        [Header("���� ��������� ���������")]
        public GameObject characterViewport;
        
        [Header("������")]
        public CreateSoldier createSoldier;
        public CreateSquad createSquad;
        
        private void Start()
        {
            OnClick(soldierEditingButton, OnSoldiersMenu);
        }

        private void OnSoldiersMenu()
        {
            Router.Main.To("CreateSoldierMenu");
        }
        
        private void OnEnable()
        {
            GenerateCharacters();
            OnClick(saveUnitButton, SaveSoldier);
        }
        //�������� ����������
        private void GenerateCharacters()
        {
            foreach (var (index, unit) in Repository.Instance.SoldiersData.Indexed())
            {
                var card = Instantiate(characterButtonPrefab, creationPosition.transform);
                listCharacters.Add(card);
                card.transform.localPosition = new Vector2(0,305 - index * 130);
                card.transform.Find("Name").GetComponent<TextMeshProUGUI>().text = unit.Name;
                //���������� ������ � ��������� ���������
                OnClick(card, () => CharacterSelection(unit.ID));
                //���������� ������ � ��������� ��������� � ���� ��������������
                OnClick(card, () => createSoldier.CharacterSelection(unit.ID));
                //���������� ������ � ��������� ��������� � ���� �������� ������
                OnClick(card, () => _unitID = unit.ID);
            }
        }

        private void SaveSoldier()
        {
            createSquad.SoldierSelection(_unitID);
            createSquad.CloseSelectionUnit();
        }
        
        //���������� ������ ����������
        private void OnDisable()
        {
            MMStartMenu.DestroyList(listCharacters);
        }
        //����������� ���������� ���������
        private void CharacterSelection(int id)
        {
            var dto = Repository.Instance.FindSoldier(id);
            SelectedCharacterName(dto.Name);
            //�������������� ���������
            Repository.Instance.SelectSoldier(characteristics, dto, magicPanel);
        }
        //����������� ����� ���������� ���������
        private void SelectedCharacterName(string characterName)
        {
            var characterNameInViewport = characterViewport.transform.Find("TopPanel/CharacterName");
            characterNameInViewport.GetComponent<TextMeshProUGUI>().text = characterName;
        }
    }
}