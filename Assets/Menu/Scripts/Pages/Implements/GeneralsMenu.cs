using System.Collections.Generic;
using Extensions;
using Menu.Scripts.Repositories;
using Menu.Scripts.Routing;
using TMPro;
using UnityEngine;

namespace Menu.Scripts.Pages.Implements
{
    public class GeneralsMenu : Page
    {
        [Header("�������� ����������")]
        public GameObject creationPosition;
        public GameObject characterButtonPrefab;
        public List<GameObject> listCharacters = new();
        
        [Header("���� ��������� ���������")]
        public GameObject characterViewport;
        
        [Header(("��������"))]
        public GameObject createCharacterButton;
        
        [Header("������")]
        public CreateGeneral createGeneral;

        [Header("��������������")] 
        public GameObject characterPhysicalDamage;
        public GameObject characterMagicDamage;
        public GameObject characterAttackSpeed;
        public GameObject characterAttackRange;
        public GameObject characterHeath;
        public GameObject characterProtection;
        public GameObject characterMagicalProtection;
        public GameObject characterMoveSpeed;
        public GameObject characterLoadCapacity;
        
        private void Start()
        {
            OnClick("TopBrowser/Back", Router.Main.Back);
            OnClick(createCharacterButton.gameObject, OnCreateGeneralMenu);
        }
        //
        private void OnCreateGeneralMenu()
        {
            Router.Main.To("CreateGeneralMenu");
        }
        
        private void OnEnable()
        {
            GenerateCharacters();
        }
        //�������� ����������
        private void GenerateCharacters()
        {
            foreach (var (index, unit) in Repository.Instance.GeneralsData.Indexed())
            {
                var card = Instantiate(characterButtonPrefab, creationPosition.transform);
                listCharacters.Add(card);
                card.transform.localPosition = new Vector2(0,310 - index * 130);
                card.transform.Find("Name").GetComponent<TextMeshProUGUI>().text = unit.Name;
                //���������� ������ � ��������� ���������
                OnClick(card, () => CharacterSelection(unit.ID));
                //���������� ������ � ��������� ��������� � ���� ��������������
                OnClick(card, () => createGeneral.CharacterSelection(unit.ID));
            }
        }
        //���������� ������ ����������
        private void OnDisable()
        {
            MMStartMenu.DestroyList(listCharacters);
        }
        //����������� ���������� ���������
        private void CharacterSelection(int id)
        {
            var dto = Repository.Instance.FindGeneral(id);
            SelectedCharacterName(dto.Name);
            
            characterPhysicalDamage.GetComponent<TextMeshProUGUI>().text = dto.PhysicalDamage.ToString();
            characterMagicDamage.GetComponent<TextMeshProUGUI>().text = dto.MagicDamage.ToString();
            characterAttackSpeed.GetComponent<TextMeshProUGUI>().text = dto.AttackSpeed.ToString();
            characterAttackRange.GetComponent<TextMeshProUGUI>().text = dto.AttackRange.ToString();
            characterHeath.GetComponent<TextMeshProUGUI>().text = dto.Health.ToString();
            characterProtection.GetComponent<TextMeshProUGUI>().text = dto.Protection.ToString();
            characterMagicalProtection.GetComponent<TextMeshProUGUI>().text = dto.MagicalProtection.ToString();
            characterMoveSpeed.GetComponent<TextMeshProUGUI>().text = dto.MoveSpeed.ToString();
            characterLoadCapacity.GetComponent<TextMeshProUGUI>().text = dto.LoadCapacity.ToString();
        }
        //��� ���������� ���������
        private void SelectedCharacterName(string characterName)
        {
            var characterNameInViewport = characterViewport.transform.Find("TopPanel/CharacterName");
            characterNameInViewport.GetComponent<TextMeshProUGUI>().text = characterName;
        }
    }
}


