using Menu.Scripts.Repositories;
using Menu.Scripts.Routing;
using TMPro;
using UnityEngine;

namespace Menu.Scripts.Pages.Implements
{
    public class CreateOfficer : Page
    {
        [Header("��������")]
        public GameObject characterName;

        [Header("��������������")] 
        public GameObject characteristics;
        public GameObject squadSizeIndicator; 

        private void Start()
        {
            OnClick("TopBrowser/Back", Router.Main.Back);
        }

        public void CharacterSelection(int dtoCharacterID)
        {
            var dto = Repository.Instance.FindOfficer(dtoCharacterID);
            SelectedCharacterName(dto.Name);
            Repository.Instance.SelectOfficer(characteristics, dto, squadSizeIndicator);
        }
        //
        private void SelectedCharacterName(string dtoCharacterName)
        {
            characterName.GetComponent<TextMeshProUGUI>().text = dtoCharacterName;
        }
    }
}