using System.Collections.Generic;
using Extensions;
using Menu.Scripts.Repositories;
using Menu.Scripts.Routing;
using TMPro;
using UnityEngine;

namespace Menu.Scripts.Pages.Implements
{
    public class OfficersMenu: Page
    {
        [Header("�������� ����������")]
        public GameObject creationPosition;
        public GameObject characterButtonPrefab;
        public List<GameObject> listCharacters = new();
        
        [Header("���� ��������� ���������")]
        public GameObject characterViewport;
        
        [Header(("��������"))]
        public GameObject createCharacterButton;
        
        [Header("������")]
        public CreateOfficer createOfficer;

        [Header("��������������")]
        public GameObject characteristics;
        public GameObject squadSizeIndicator;
        
        private void Start()
        {
            OnClick("TopBrowser/Back", Router.Main.Back);
            OnClick(createCharacterButton.gameObject, OnCreateOfficerMenu);
        }
        //
        private void OnCreateOfficerMenu()
        {
            Router.Main.To("CreateOfficerMenu");
        }
        
        private void OnEnable()
        {
            GenerateCharacters();
        }
        //�������� ����������
        private void GenerateCharacters()
        {
            foreach (var (index, unit) in Repository.Instance.OfficersData.Indexed())
            {
                var card = Instantiate(characterButtonPrefab, creationPosition.transform);
                listCharacters.Add(card);
                card.transform.localPosition = new Vector2(0,310 - index * 130);
                card.transform.Find("Name").GetComponent<TextMeshProUGUI>().text = unit.Name;
                //���������� ������ � ��������� ���������
                OnClick(card, () => CharacterSelection(unit.ID));
                //���������� ������ � ��������� ��������� � ���� ��������������
                OnClick(card, () => createOfficer.CharacterSelection(unit.ID));
            }
        }
        //���������� ������ ����������
        private void OnDisable()
        {
            MMStartMenu.DestroyList(listCharacters);
        }
        //����������� ���������� ���������
        private void CharacterSelection(int id)
        {
            var dto = Repository.Instance.FindOfficer(id);
            SelectedCharacterName(dto.Name);
            Repository.Instance.SelectOfficer(characteristics, dto, squadSizeIndicator);
        }
        //��� ���������� ���������
        private void SelectedCharacterName(string characterName)
        {
            var characterNameInViewport = characterViewport.transform.Find("TopPanel/CharacterName");
            characterNameInViewport.GetComponent<TextMeshProUGUI>().text = characterName;
        }
    }
}


