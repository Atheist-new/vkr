using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Menu.Scripts.Pages
{
    public class Page : MonoBehaviour
    {
        public void OnClick(string pathObject, UnityAction callable)
        {
            var obj = transform.Find(pathObject).gameObject;
            obj.GetComponent<Button>().onClick
                .AddListener(callable);
        }
        public void OnClick(GameObject obj, UnityAction callable)
        {
            obj.GetComponent<Button>().onClick
                .AddListener(callable);
        }

        public virtual void Show()
        {
            gameObject.SetActive(true);
        }
        
        public virtual void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}