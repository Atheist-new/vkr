using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime;
using Unity.Mathematics;

namespace DataClass
{
    public class BaseUnit : BaseObject
    {
        private int fraction = 0;   //ID �������
        private int experience = 0; //���� �������� �����
        private MagicBranch magicBranch = MagicBranch.Water;
        private BonusEffect effectsLvlup = new BonusEffect();   //������ ���������� �� ������ ������� ���� �����
        private StatBlock resultStats = new StatBlock();
        private List<Skill> unitSkill = new List<Skill>(2);   //���� ������������ �����, ������ 0 - ��������, 1 - �������� 
        public int Fraction { get => fraction; }
        public int Experience { get => experience; }
        public MagicBranch MagicBranch { get => magicBranch; }
        public BonusEffect EffectsLvlup { get => effectsLvlup; }
        public StatBlock ResultStats { get => resultStats;}
        public List<Skill> UnitSkill { get => unitSkill; }

        public StatBlock calculationStats()
        {
            int lvl = StaticMetod.calcLvl(this.Experience);
            this.resultStats.AttackType = this.Stats.AttackType;
            this.resultStats.ArmorType = this.Stats.ArmorType;
            foreach (var stat in this.resultStats.DictStat)
            {
                this.resultStats.DictStat[stat.Key] = this.Stats.DictStat[stat.Key] * math.pow(1 + this.effectsLvlup.DictMod[stat.Key], lvl);
            }
            return this.resultStats;
        }
    }
}
