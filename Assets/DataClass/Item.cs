using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using System.Linq;

namespace DataClass
{
    public class Item : BaseObject
    {
        #region param
        private int tipeItemID = 0;         //ID ���� ��������
        private BonusEffect mod = new BonusEffect();            //������������ ��������

        private int experience = 0;         //���� �������� ��������
        private BonusEffect effectsLvlup = new BonusEffect();   //������ ���������� �� ������ ������� ��������
        private int expStar = 0;            //�������� ���������
        private List<StatBlock> effectsStarStat = new List<StatBlock>(10);  //���������� ���������� �� ����������� ������� ������
        private List<BonusEffect> effectsStarMod = new List<BonusEffect>(10);   //������������ ���������� �� ����������� ������� ������
        private StatBlock resultStats = new StatBlock();
        private BonusEffect resultMod = new BonusEffect();
        #endregion
        #region prop
        public int TipeItemID { get => tipeItemID; }
        public BonusEffect Mod { get => mod; }
        public int Experience { get => experience; }
        public BonusEffect EffectsLvlup { get => effectsLvlup; }
        public int ExpStar { get => expStar; }
        public List<StatBlock> EffectsStarStat { get => effectsStarStat; }
        public List<BonusEffect> EffectsStarMod { get => effectsStarMod; }
        public StatBlock ResultStats { get => resultStats; }
        public BonusEffect ResultMod { get => resultMod; }
        #endregion
        public StatBlock calculationStats()
        {
            int lvl = StaticMetod.calcLvl(this.Experience);
            int lvlStar = StaticMetod.calcLvl(this.expStar);
            
            foreach(var stat in this.resultStats.DictStat)
            {
                this.resultStats.DictStat[stat.Key] = (
                    this.Stats.DictStat[stat.Key] +    //�������� �������� ���������
                    effectsStarStat //��������� ���������� �� ����
                    .Take(lvlStar)
                    .Select(block => block.DictStat[stat.Key])
                    .Aggregate(0f, (acc, x) => acc + x)
                ) * math.pow(1 + this.effectsLvlup.DictMod[stat.Key], lvl);//��������� �� �������
            }
            return this.resultStats;
        }
        public StatBlock calculationMod()
        {
            int lvlStar = StaticMetod.calcLvl(this.expStar);
            foreach (var stat in this.resultStats.DictStat)
            {
                this.resultMod.DictMod[stat.Key] = 
                this.Mod.DictMod[stat.Key] + 
                effectsStarMod.Take(lvlStar).Select(block => block.DictMod[stat.Key]).Aggregate(0f, (acc, x) => acc + x);
            }
            return this.resultStats;
        }
    }
}