using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataClass
{
    public class BaseObject
    {
        private string name;        //��� �������
        private string description; //�������� �������
        private StatBlock stats;    //��������������� �������
        private Cost cost;          //��������� ��������� �������
        private int modelID;        //ID ��������
        private int iconID;         //ID ������
        public string Name { get => name; }
        public string Description { get => description; }
        public StatBlock Stats { get => stats; }
        public Cost Cost { get => cost; }
        public int ModelID { get => modelID; }
        public int IconID { get => iconID; }

    }

}