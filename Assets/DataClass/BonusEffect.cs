using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataClass
{
    public class BonusEffect
    {
        #region field
        private string name;                //�������� �������
        private string description;         //�������� �������
        private int iconID;                 //ID ������
        public Dictionary<string, float> dictMod = new Dictionary<string, float>()
        {
            { "Damage", 0 },            //����������� ����������� �����
            { "MagicDamageWoter", 0 },  //����������� ����������� �����
            { "MagicDamageFier", 0 },   //����������� ����������� �����
            { "MagicDamageEarth", 0 },  //����������� ����������� �����
            { "MagicDamageWind", 0 },   //����������� ����������� �����
            { "AttackSpeed", 0 },       //����������� �������� �����
            { "AttackRange", 0 },       //����������� ��������� �����
            { "Health", 0 },            //����������� ����� ��������
            { "Armor", 0 },             //����������� ������
            { "MagicResist", 0 },       //����������� ������ �� �����
            { "Moral", 0 },             //����������� ������
            { "Discipline", 0 },        //����������� ����������
            { "MoveSpeed", 0 },         //����������� �������� ������������
            { "LoadCapacity", 0 },      //����������� ����������������
            { "BuildTime", 0 }          //����������� ������� ��������� �������
        };
        #endregion
        #region Props
        public string Name { get => name; }
        public string Description { get => description; }
        public int IconID { get => iconID; }
        public Dictionary<string, float> DictMod { get => dictMod; }
        #endregion
    }
}
