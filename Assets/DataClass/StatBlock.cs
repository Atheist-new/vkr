using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Specialized;

namespace DataClass
{
    public class StatBlock
    {
        public int AttackType { get; set; } = 0;        //ID ���� �����
        public int ArmorType { get; set; } = 0;         //ID ���� ������
        public Dictionary<string, float> DictStat { get; set; } = new Dictionary<string, float>()
        {
            { "Damage", 0 },            //���������� ���� ��� �����
            { "MagicDamageWoter", 0 },  //���������� ���� ����� ����
            { "MagicDamageFier", 0 },   //���������� ���� ����� ����
            { "MagicDamageEarth", 0 },  //���������� ���� ����� �����
            { "MagicDamageWind", 0 },   //���������� ���� ����� �������
            { "AttackSpeed", 0 },       //�������� �����
            { "AttackRange", 0 },       //������ �����
            { "InteractionRange", 0 },  //������ ��������������
            { "ViewRange", 0 },         //������ ������ (������ ����)
            { "Health", 0 },            //������������ ��������
            { "Armor", 0 },             //������
            { "MagicResist", 0 },       //������ �� �����
            { "Moral", 0 },             //������������ ���� ������� ����
            { "Discipline", 0 },        //����������
            { "MoveSpeed", 0 },         //�������� �����������
            { "LoadCapacity", 0 },      //����������������
            { "BuildTime", 0 }          //����� ��������� ������� � ��������
        };
    }
}