using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataClass
{
    public static class StaticMetod
    {
        public static int calcLvl(int exp)
        {
            int lvl = 0;
            int sExp = 0;
            int constLvl = 15;
            double mult = 0.15;
            while (exp > sExp)
            {
                int p = constLvl * lvl + (int)(sExp * mult / 5) * 5;
                sExp += (int)p;
                lvl++;
            }
            return lvl;
        }
    }
    public enum MagicBranch
    {
        �ommander,
        Water,
        Fier,
        Earth,
        Wind
    }
    public enum TypeSkill
    {
        Active,
        AutoCast,
        Passive
    }
}
