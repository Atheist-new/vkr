using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataClass
{
    public class Cost
    {
        public Dictionary<string, int> DirCost { get; set; } = new Dictionary<string, int>()
        {
            { "ManaCrystal", 0 },    //��������� ���������� ���� � ��������
            { "Gold", 0 },          //��������� ������ � ��������
            { "Provisions", 0 },    //��������� ���� � ��������
            { "Wood", 0 },          //��������� ������ � ��������
            { "Stone", 0 },         //��������� ����� � ��������
            { "Metal", 0 }         //��������� ������� � ��������
        };
    }
}