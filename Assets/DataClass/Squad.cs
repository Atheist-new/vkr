using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataClass
{
    public class Squad
    {

        private Unit unit;

        private Character officer;
        public Unit Unit { get => unit; }
        public Character Officer { get => officer; }
    }
}