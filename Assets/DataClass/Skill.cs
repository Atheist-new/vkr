using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataClass
{
    public class Skill
    {
        private string name;        //��� �����������
        private string description; //�������� �����������
        private int effectID;       //ID �����-����������� �������
        private int iconID;         //ID ������
        private float cooldown;     //����������� �����������
        private MagicBranch magicBranch = MagicBranch.�ommander;    //���������� ����� (0 - �� �����)
        private TypeSkill typeSkill = TypeSkill.Passive;            //��� �����������
        public string Name { get => name; }
        public string Description { get => description; }
        public int EffectID { get => effectID; }
        public int IconID { get => iconID; }
        public float Cooldown { get => cooldown; }
        public MagicBranch MagicBranch { get => magicBranch;}
        public TypeSkill TypeSkill { get => typeSkill;}
    }
}