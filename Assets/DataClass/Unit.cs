using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataClass
{
    public class Unit : BaseObject
    {
        private BaseUnit baseUnit = new BaseUnit();      //���� �����
        private Item weapon = new Item();            //������������� ������
        private Item equipment = new Item();         //������������� �����
        private Item mount = new Item();             //������� ��������
        private StatBlock resultStats = new StatBlock();   //�������������� ����������
        public BaseUnit BaseUnit { get => baseUnit; }
        public Item Weapon { get => weapon;}
        public Item Equipment { get => equipment;}
        public Item Mount { get => mount;}
        public StatBlock ResultStats { get => resultStats;}

        public StatBlock calculationStats()
        {
            this.Stats.AttackType = weapon.Stats.AttackType;
            this.Stats.ArmorType = equipment.Stats.ArmorType;
            foreach (var stat in this.Stats.DictStat)
            {
                this.Stats.DictStat[stat.Key] = (this.BaseUnit.ResultStats.DictStat[stat.Key]
                    + this.weapon.ResultStats.DictStat[stat.Key]
                    + this.equipment.ResultStats.DictStat[stat.Key]
                    + this.mount.ResultStats.DictStat[stat.Key])
                    * (1 + this.weapon.ResultMod.DictMod[stat.Key] 
                         + this.equipment.ResultMod.DictMod[stat.Key] 
                         + this.mount.ResultMod.DictMod[stat.Key]);
            }
            return this.resultStats;
        }

    }
}