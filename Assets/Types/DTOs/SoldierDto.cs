using UnityEngine;

namespace Types.DTOs
{
    public class SoldierDto
    {
        public int ID;
        public string Name;
        public int PhysicalDamage;
        public int MagicDamage;
        public int AttackSpeed;
        public int AttackRange;
        public int Health;
        public int Protection;
        public int MagicalProtection;
        public int MoveSpeed;
        public int LoadCapacity;
        public Color MagicSkill1;
        public Color MagicSkill2;
        
        public SoldierDto(int idDto, string nameDto, int physicalDamageDto, int magicDamageDto, int attackSpeedDto, int attackRangeDto,
            int healthDto, int protectionDto, int magicProtectionDto, int moveSpeedDto, int loadCapacityDto, Color magicSkill1Dto, Color magicSkill2Dto)
        {
            ID = idDto;
            Name = nameDto;
            PhysicalDamage = physicalDamageDto;
            MagicDamage = magicDamageDto;
            AttackSpeed = attackSpeedDto;
            AttackRange = attackRangeDto;
            Health = healthDto;
            Protection = protectionDto;
            MagicalProtection = magicProtectionDto;
            MoveSpeed = moveSpeedDto;
            LoadCapacity = loadCapacityDto;
            MagicSkill1 = magicSkill1Dto;
            MagicSkill2 = magicSkill2Dto;
        }
    }
}
