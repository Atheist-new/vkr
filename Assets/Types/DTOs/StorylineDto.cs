namespace Types.DTOs
{
    public class StorylineDto
    {
        public int Id;
        public string Name;

        public StorylineDto(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}