namespace Types.DTOs
{
    public class SquadsDto
    {
        public int IdSquad;
        public string NameSquad;
        public int IdOfficer;
        public int IdSoldier;
        public float CurrentSquadSize;
        
        public SquadsDto(int idDto, string nameDto, int idSoldierDto, int idOfficerDto, float currentSquadSizeDto)
        {
            IdSquad = idDto;
            NameSquad = nameDto;
            IdSoldier = idSoldierDto;
            IdOfficer = idOfficerDto;
            CurrentSquadSize = currentSquadSizeDto;
        }
    }
}