namespace Types.DTOs
{
    public class GeneralsDto
    {
        public int ID;
        public string Name;
        public int PhysicalDamage;
        public int MagicDamage;
        public int AttackSpeed;
        public int AttackRange;
        public int Health;
        public int Protection;
        public int MagicalProtection;
        public int MoveSpeed;
        public int LoadCapacity;
        
        public GeneralsDto(int idDto, string nameDto, int physicalDamageDto, int magicDamageDto, int attackSpeedDto, int attackRangeDto,
            int healthDto, int protectionDto, int magicProtectionDto, int moveSpeedDto, int loadCapacityDto)
        {
            ID = idDto;
            Name = nameDto;
            PhysicalDamage = physicalDamageDto;
            MagicDamage = magicDamageDto;
            AttackSpeed = attackSpeedDto;
            AttackRange = attackRangeDto;
            Health = healthDto;
            Protection = protectionDto;
            MagicalProtection = magicProtectionDto;
            MoveSpeed = moveSpeedDto;
            LoadCapacity = loadCapacityDto;
        }
    }
}