using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Gameplay
{
    public class MasterTargeter : Targeter
    {
        protected List<Targeter> targeters;

        protected override void Start()
        {
            base.Start();

            // Список других таргетеров на данном объекте, сортируем по убыванию приоритета
            targeters = GetComponents<Targeter>()
                .Where(it => it != this)
                .OrderByDescending(it => it.Priority)
                .ToList();
        }

        protected override void Update()
        {
            base.Update();
            
            if (pathfinder.Target == null)
                target = null;
            
            // TODO делать это не каждый кадр??

            foreach (var targeter in targeters)
            {
                // Нет смысла смотреть дальше, текущую цель мы не перекроем
                if (target != null && targeter.Priority < target.Priority) break;

                Target newTarget = targeter.GetTarget();

                // Нету цели, идём дальше
                if (newTarget == null || newTarget.State != TargetState.Waiting) continue;
                if (newTarget == target) break;

                // Есть цель и приоритет круче
                if (target == null || targeter.Priority >= target.Priority)
                {
                    SetTarget(newTarget);
                    break;
                }
            }
        }
    }
}
