using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Gameplay
{
    public class AreaTargeter : Targeter
    {
        [SerializeField]
        [Header("Area Targeter")]
        protected bool checkMobile = true;

        [SerializeField]
        protected bool checkAlignment = true;

        protected List<MapObject> targetsNearby;

        protected MapObject self;

        protected override void Start()
        {
            base.Start();

            targetsNearby = new();
            self = GetComponent<MapObject>();
        }

        protected override void Update()
        {
            base.Update();
            
            targetsNearby = targetsNearby.Where(mo => mo != null).ToList();

            if (targetsNearby.Count == 0) return;

            MapObject closest = targetsNearby.OrderBy(
                mo => Vector3.Distance(mo.transform.position, transform.position)
            ).First();

            SetTarget(closest);
        }

        public void OnAreaEnter(MapObject other)
        {
            if (checkMobile && !other.Mobile) return;
            if (checkAlignment && self.Alignment == other.Alignment) return;

            targetsNearby.Add(other);
        }

        public void OnAreaExit(MapObject obj)
        {
            targetsNearby.Remove(obj);
        }
    }
}
