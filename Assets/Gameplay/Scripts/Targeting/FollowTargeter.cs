using System.Collections;
using System.Collections.Generic;
using Extensions;
using Gameplay;
using UnityEngine;

namespace Gameplay
{
    public class FollowTargeter : Targeter
    {
        [SerializeField]
        private MapObject targetObject;

        [SerializeField]
        private float updateDelay = 0.1f;

        [SerializeField] [ReadOnly]
        private float elapsed;

        protected override void Start()
        {
            base.Start();

            elapsed = updateDelay;
        }
        
        protected override void Update()
        {
            base.Update();

            elapsed += Time.deltaTime;
            
            if (elapsed < updateDelay) return;

            elapsed -= updateDelay;
            
            var distance = (transform.position - targetObject.transform.position).magnitude;

            if (distance > 0.05f)
            {
                SetTarget(targetObject);
            }
        }
        
        protected override bool ShouldKeepTarget(MapObject obj)
        {
            return false;
        }
    }
}
