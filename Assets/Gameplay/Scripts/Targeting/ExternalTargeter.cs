using System.Collections;
using System.Collections.Generic;
using Unity.Burst.CompilerServices;
using UnityEngine;

namespace Gameplay
{
    public class ExternalTargeter : Targeter
    {
        protected override void Update()
        {
            base.Update();
            
            // Сбросить цель, если она достигнута или отменена
            if (target != null && target.State >= TargetState.Reached) 
                target = null;
        }

        public new void SetTarget(MapObject target)
        {
            base.SetTarget(target);
        }

        public void SetTarget(Vector3 pos)
        {
            MapObject flag = Map.main.CreateFlag(pos);
            flag.Align(AlignmentSystem.main.UserAlignment);

            base.SetTarget(flag);
        }
    }
}