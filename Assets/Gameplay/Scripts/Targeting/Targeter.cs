using Gameplay.Pathing;
using UnityEditor;
using UnityEngine;

namespace Gameplay
{
    public class Targeter : MonoBehaviour
    {
        [SerializeField]
        protected bool passive = false;

        [SerializeField]
        protected int priority = 0;

        public int Priority { get => priority; }

        public bool Passive { get => passive; set => passive = value; }

        protected Target target = null;

        [Header("Debug")]
        [SerializeField] [ReadOnly]
        private bool targetSet = false;
        
        [SerializeField] [ReadOnly]
        private TargetInfo targetInfo = null;

        protected Pathfinder pathfinder;

        protected virtual void Awake()
        {
            pathfinder = GetComponent<Pathfinder>();
        }

        protected virtual void Start() { }

        protected virtual void Update()
        {
            // TODO удалить после дебага
            targetSet = target != null;
            targetInfo = target?.Info;
        }

        protected virtual bool ShouldKeepTarget(MapObject obj)
        {
            return (
                target != null
                && target.Object == obj // Цель та же самая
                && target.State < TargetState.Reached // И она не достигнута
            );
        }
        
        protected virtual void SetTarget(MapObject obj)
        {
            if (ShouldKeepTarget(obj)) return;

            SetTarget(new Target(obj, priority, this));
        }

        protected virtual void SetTarget(Target newTarget)
        {
            target = newTarget;

            // TODO удалить после дебага
            targetSet = newTarget != null;
            targetInfo = newTarget?.Info;

            if (!passive)
                pathfinder.SetTarget(this.target);
        }

        public virtual Target GetTarget() => target;

        public virtual void ResetTargeter() { }

        public void OnDrawGizmosSelected()
        {
            if (target != null)
            {
                Handles.color = Color.magenta;
                Handles.DrawWireDisc(target.Position, Vector3.up, 1f);
            }
        }
    }
}