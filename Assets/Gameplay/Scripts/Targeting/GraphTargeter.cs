using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;
using System.Linq;

namespace Gameplay
{
    public class GraphTarget : Target
    {
        public Vector3 Offset;
        public override Vector3 Position => base.Position + Offset;

        public GraphTarget(MapObject obj, int priority, Targeter targeter, float radius = 0) 
            : base(obj, priority, targeter)
        {
            Offset = radius.RandomOffset().ToVector3();
        }
    }
    
    public class GraphTargeter : Targeter
    {
        [Header("GraphTargeter")]
        [SerializeField] [ReadOnly]
        private GraphNode current;

        [SerializeField]
        private bool searchClosest = false;

        public GraphNode Current { get => current; set => current = value; }

        private float elapsed = 0;

        // TODO это какой-то костыль получается, нужно как-то по другому это организовать
        public bool Inverse { get; set; } = false;
        
        protected override void Update()
        {
            base.Update();
            
            elapsed += Time.deltaTime;

            if (elapsed > 0.3f)
                elapsed -= 0.3f;
            else
                return;

            // Debug.Log($"{this}: target is {target}, state is {target?.State}");
            // Не ищем новую цель, если текущая активна
            if (target != null && target.State <= TargetState.Active) return;
            
            // Ближайший узел ищем в двух случаях:
            // 1. Текущий узел не задан - тут без вариантов, надо найти хоть какой-нибудь
            // 2. searchClosest установлен и мы не достигли предыдущей цели
            // Во втором случае, считаем, что если цели нет, то она достигнута
            
            var targetNotReached = target != null && target.State != TargetState.Reached;
            if (current == null || searchClosest && targetNotReached)
            {
                // Debug.Log($"{this}: Finding closest node");
                current = FindClosestNode();
            }
            else
            {
                // Debug.Log($"{this}: Finding best next node");
                current = SelectBestNeighbour();
            }

            SetTarget(current);
            // Debug.Log($"{this}: Set new target to {target}");
        }

        protected GraphNode FindClosestNode()
        {
            // TODO выбирать один из 2-3 ближайших узлов по приоритету

            var nodes = FindObjectsOfType<GraphNode>();

            return nodes.OrderBy(
                node => Vector3.Distance(transform.position, node.transform.position)
            ).First();
        }

        protected GraphNode SelectBestNeighbour()
        {
            // Выбираем слуайный узел из соседних узлов с наивысшим приоритетом
            return current.Nodes.MaxAll(
                node => node.GetPriority(
                    this.mapObject().Alignment, 
                    inverse: Inverse
                )
            ).Random();
        }

        protected virtual void SetTarget(GraphNode node)
        {
            MapObject mo = node.mapObject();   
            
            if (ShouldKeepTarget(mo)) return;

            SetTarget(new GraphTarget(mo, priority, this, node.Radius));
        }
        
        protected override void SetTarget(MapObject obj)
        {
            if (ShouldKeepTarget(obj)) return;

            SetTarget(new GraphTarget(obj, priority, this));
        }
    }
}