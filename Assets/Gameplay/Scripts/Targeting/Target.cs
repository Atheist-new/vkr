using System;
using UnityEngine;

namespace Gameplay
{
    public enum TargetState 
    {
        NotSet,
        Waiting,
        Active, 
        Reached, 
        Canceled
    }

    [Serializable]
    public class TargetInfo
    {
        public MapObject mapObject;
        public int priority;
        public TargetState state;
        public Targeter targeter;
    }
    
    public class Target
    {
        public MapObject Object { get; protected set; }

        public int Priority { get; protected set; }

        public Targeter Targeter { get; protected set; }

        public TargetState State { get; protected set; } = TargetState.Waiting;

        public TargetInfo Info => new()
        {
            mapObject = Object,
            priority = Priority,
            state = State,
            targeter = Targeter
        };

        public virtual Vector3 Position => Object.Position;
        
        public Target(MapObject obj, int priority, Targeter targeter)
        {
            Object = obj;
            Priority = priority;
            Targeter = targeter;

            // Debug.Log($"{Object}: Set as Target");
        }

        public virtual void OnTargetSet()
        {
            State = TargetState.Active;

            // Debug.Log($"{Object}: Target Active");
        }
        public virtual void OnTargetReached() 
        {
            if (State != TargetState.Reached)
            {
                State = TargetState.Reached;

                // Debug.Log($"{Object}: Target Reached");

                OnTargetRemoved();
            }
        }
        public virtual void OnTargetCanceled() 
        {
            if (State != TargetState.Canceled)
            {
                State = TargetState.Canceled;

                // Debug.Log($"{Object}: Target Canceled");

                OnTargetRemoved();
            }
        }
        public virtual void OnTargetRemoved()
        {
            // Debug.Log($"{Object}: Target Removed");

            Object.OnRemoveTarget();
        }

        public static bool operator ==(Target obj1, Target obj2)
        {
            if (ReferenceEquals(obj1, obj2)) 
                return true;
            
            if (obj1 is not null && obj2 is null)
            {
                return obj1.Object == null;
            }

            if (obj1 is null && obj2 is not null)
            {
                return obj2.Object == null;
            }

            return obj1.Object == obj2.Object;
        }
        public static bool operator !=(Target obj1, Target obj2) => !(obj1 == obj2);

        public override string ToString()
        {
            return $"{Object}[{Priority} {State}]";
        }
    }
}
