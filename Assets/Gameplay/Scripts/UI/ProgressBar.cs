using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Gameplay
{
    public class ProgressBar : MonoBehaviour
    {
        [SerializeField]
        private Slider slider;

        public void SetProgress(float percent)
        {
            slider.value = percent;
        }

        public void SetProgress(float current, float max)
        {
            slider.value = current / max;
        }
    }
}