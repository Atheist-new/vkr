using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Extensions;
using System.Linq;

namespace Gameplay
{
    public class GraphNode : MonoBehaviour
    {
        [SerializeField]
        private List<GraphNode> nodes = new();
        public List<GraphNode> Nodes { get => nodes; }

        [SerializeField]
        private int basePriority = 0;

        [SerializeField]
        private float radius = 1.15f;

        public float Radius => radius;
        
        public int GetPriority(int alignment, int depth = 1, bool inverse = false)
        {
            var priority = basePriority * GetFactor(alignment);

            if (inverse)
                priority *= -1;

            if (depth <= 0) return priority;

            return 2 * priority + nodes.Select(
                node => node.GetPriority(alignment, depth - 1, inverse)
            ).Sum();
        }

        private int GetFactor(int alignment)
        {
            int other = this.mapObject().Alignment;

            if (other == 0) return 1;
            if (alignment == other) return -1;

            return 2;
        }

        private void OnDrawGizmos()
        {
            DrawLines(Color.gray);
        }

        private void OnDrawGizmosSelected()
        {
            DrawLines(Color.green, 5);
            Handles.DrawWireDisc(
                transform.position.withY(Map.GetHeightAt(transform.position)), 
                Vector3.up, radius
                );
        }

        private void DrawLines(Color color, float thickness = 1)
        {
            foreach (GraphNode node in nodes)
            {
                if (node != null)
                {
                    Handles.DrawBezier(
                        transform.position, node.transform.position,
                        transform.position, node.transform.position,
                        color, null, thickness
                    );
                }
            }
        }
    }
}