using Gameplay;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

namespace Gameplay
{
    public class BattleSystem : MonoBehaviour
    {
        #region Singleton
        private static BattleSystem instance;
        public static BattleSystem main { get => instance; }
        private void Awake()
        {
            instance = this;
        }
        #endregion // Singleton

        public List<(Formation att, Formation def)> battles = new();

        [SerializeField]
        private float tickLength = 0.25f;

        [SerializeField]
        [ReadOnly]
        [Header("Debug")]
        private int currentTick = 0;

        [SerializeField]
        [ReadOnly]
        private float elapsed;

        [SerializeField]
        [ReadOnly]
        private int battleCount = 0;

        public void InitiateBattle(Formation attacker, Formation defender)
        {
            if (attacker == null || defender == null)
                return;

            if (!attacker.CanFight || !defender.CanFight)
                return;

            // Уже деремся
            if (battles.Any(pair => pair.att == attacker))
                return;

            // Debug.Log($"New battle {attacker} vs {defender}");

            if (battles.Any(pair => pair.att == defender))
            {
                battles.Add((attacker, defender));
                attacker.OnBattleStarted();
                return;
            }

            battles.Add((attacker, defender));
            battles.Add((defender, attacker));  // TODO возможно отряды не будут атаковать в ответ
            attacker.OnBattleStarted();
            defender.OnBattleStarted();
        }

        private void FixedUpdate()
        {
            battleCount = battles.Count();

            elapsed += Time.deltaTime;

            if (elapsed > tickLength)
            {
                elapsed -= tickLength;

                switch (currentTick)
                {
                    case 0: ArcheryTick(); break; 
                    case 1: CavalryTick(); break; 
                    case 2: MeeleTick(); break; 
                    case 3: AutocastTick(); break; 
                }

                PostTick();

                currentTick = (currentTick + 1) % 4;
            }
        }

        private void ArcheryTick() { }
        private void CavalryTick() { }
        private void MeeleTick() 
        {
            List<(Formation f, float damage)> takenDamage = new();

            foreach (var (att, def) in battles)
            {
                // TODO проверить тип формирования
                float damage = att.DealDamage(def.Armor);
                takenDamage.Add((def, damage));

                // Debug.Log($"{att} deals {damage} damage");
            }

            // TODO объединить в один цикл?
            foreach (var (f, damage) in takenDamage)
            {
                f.OnTakeDamage(damage);

                // Debug.Log($"{f} receives {damage} damage");
            }
        }
        private void AutocastTick() { }

        private void PostTick()
        {
            HashSet<Formation> free = new();
            HashSet<Formation> eliminated = new();

            for (int i = battles.Count - 1; i >= 0; i--)
            {
                var battle = battles[i];

                if (!battle.att.CanFight)
                {
                    battles.RemoveAt(i);
                    eliminated.Add(battle.att);

                    continue;
                }

                if (!battle.def.CanFight)
                {
                    battles.RemoveAt(i);

                    if (battle.att.CanFight)
                        free.Add(battle.att);

                    continue;
                }
            }

            foreach (var formation in eliminated)
            {
                if (formation != null)
                {
                    // Debug.Log($"{formation} is eliminated");
                    formation.OnBattleEnded();
                }
            }

            foreach (var formation in free)
            {
                Formation opponent = battles
                    .Where(b => b.def == formation)
                    .Select(b => b.att)
                    .OrderBy(op => Vector3.Distance(formation.transform.position, op.transform.position))
                    .FirstOrDefault();

                if (opponent == null)
                {
                    // Debug.Log($"{formation} wins");
                    formation.OnBattleEnded();
                    
                }
                else
                {
                    // Debug.Log($"{formation} attaks {opponent}");
                    battles.Add((formation, opponent));
                }
            }
        }
    }
}