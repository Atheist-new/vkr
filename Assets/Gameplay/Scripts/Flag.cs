using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class Flag : MapObject
    {
        public override void OnRemoveTarget()
        {
            Destroy(gameObject);
        }
    }
}