using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DataClass;
using Extensions;

namespace Gameplay
{
    public enum FormationState
    {
        Free,
        Marching,
        Battle,
        Capture
    }

    public class Formation : MonoBehaviour
    {
        protected StatBlock statBlock = new();

        [SerializeField]
        protected float rotationRate = 5f;
        
        [SerializeField] [ReadOnly]
        protected FormationState state = FormationState.Free;

        [SerializeField]
        protected bool freeze = false;

        public FormationState State { get => state; set => state = value; }

        [SerializeField]
        protected ProgressBar healthBar;

        public float Health => statBlock.DictStat["Health"];
        public float Damage => statBlock.DictStat["Damage"];
        public float Speed => statBlock.DictStat["Speed"];
        public float AttackSpeed => statBlock.DictStat["AttackSpeed"];
        public float AttackRange => statBlock.DictStat["AttackRange"];
        public float InteractionRange => statBlock.DictStat["InteractionRange"];
        public float ViewRange => statBlock.DictStat["ViewRange"];
        public float Armor => statBlock.DictStat["Armor"];
        public float RotationSpeed => Speed * rotationRate;

        // TODO проверять текущее состояние
        public virtual bool ShouldMove => !freeze && State == FormationState.Free;

        public virtual bool CanFight => true;
        public virtual bool NeedsHealing => false;

        public float GetStat(string name)
        {
            return statBlock.DictStat[name];
        }

        public void OnAttackRangeStay(MapObject other) 
        {
            if (state == FormationState.Battle) return;

            MapObject self = GetComponent<MapObject>();

            if (other.Mobile && self.Alignment != other.Alignment)
            {
                StartBattle(other);
            }
        }

        protected virtual void StartBattle(MapObject opponent)
        {
            BattleSystem.main.InitiateBattle(this, opponent.GetComponent<Formation>());
        }

        public virtual void OnBattleStarted()
        {
            state = FormationState.Battle;
        }

        public virtual void OnBattleEnded()
        {
            state = FormationState.Free;

            if (Health <= 0) 
                Destroy(gameObject);
        }

        public virtual bool OnTakeDamage(float damage)
        {
            return false;
        }

        public virtual float DealDamage(float defense)
        {
            return 0;
        }

        public virtual void StartCapture()
        {
            if (state >= FormationState.Battle) return;

            state = FormationState.Capture;
        }

        public virtual void EndCapture()
        {
            state = FormationState.Free;
        }
        
        public virtual void Heal() {}
    }
}