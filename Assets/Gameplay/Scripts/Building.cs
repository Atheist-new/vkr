using System;
using System.Collections.Generic;
using System.Linq;
using Extensions;
using Gameplay.Pathing;
using UnityEngine;
using UnityEngine.Events;

namespace Gameplay
{
    public enum CaptureState
    {
        Stale,
        Capture,
        Release
    }
    
    [Serializable]
    public class BuildingCapturedEvent : UnityEvent<int> { }
    
    public class Building : MonoBehaviour
    {
        [SerializeField]
        private float captureSpeed = 1f/30f;
        
        [SerializeField] [ReadOnly]
        private float captureProgress = 0;
        
        [SerializeField] [ReadOnly]
        private CaptureState captureState = CaptureState.Stale;

        [SerializeField]
        private ProgressBar captureBar;

        [Header("Events")]
        public BuildingCapturedEvent BuildingCaptured;
        
        private List<MapObject> unitsNearby = new();
        
        protected MapObject self;
        
        // Start is called before the first frame update
        void Start()
        {
            self = GetComponent<MapObject>();
        }

        // Update is called once per frame
        void Update()
        {
            unitsNearby = unitsNearby.Where(unit => unit != null).ToList();
            
            var enemiesNearby = unitsNearby.Count(unit => unit.Alignment != self.Alignment);
            var alliesNearby = unitsNearby.Count(unit => unit.Alignment == self.Alignment);

            if (enemiesNearby > 0 && alliesNearby <= 0)
            {
                captureState = CaptureState.Capture;
            }
            else if (enemiesNearby <= 0)
            {
                captureState = CaptureState.Release;
            }
            else
            {
                captureState = CaptureState.Stale;
            }

            ProgressCapture(captureState, enemiesNearby, alliesNearby);

            // TODO нужна ли эта проверка?
            if (enemiesNearby <= 0)
            {
                // Отхил союзных юнитов
                unitsNearby
                    .Where(unit => unit.Alignment == self.Alignment)
                    .Apply(unit =>
                    {
                        if (!unit.TryGetComponent(out Formation formation)) return;
                        if (!formation.NeedsHealing) return;
                        
                        formation.Heal();
                        // TODO опять немного костыльно выходит
                        formation.GetComponent<Pathfinder>().ClearTarget(true);
                    });
            }
        }

        private void ProgressCapture(CaptureState state, int enemies = 0, int allies = 0)
        {
            switch (state)
            {
                case CaptureState.Capture:
                {
                    captureProgress += captureSpeed * Time.deltaTime * enemies;
                    
                    if (captureProgress >= 1)
                    {
                        OnBuildingCaptured();
                    }

                    break;
                }
                case CaptureState.Release:
                {
                    captureProgress -= captureSpeed * Time.deltaTime * (allies + 1);
                    
                    if (captureProgress < 0)
                    {
                        captureProgress = 0;
                    }

                    break;
                }
            }

            captureBar.SetProgress(captureProgress);
        }

        private void OnBuildingCaptured()
        {
            var alignment = unitsNearby.First(unit => unit.Alignment != self.Alignment).Alignment;
            
            self.Align(alignment);

            unitsNearby
                .Where(unit => unit.Alignment == self.Alignment)
                .Apply(unit => unit.GetComponent<Formation>().EndCapture());

            captureProgress = 0;
            
            BuildingCaptured.Invoke(alignment);
        }
        
        public void OnAreaEnter(MapObject other)
        {
            if (!other.Mobile) return;

            unitsNearby.Add(other);
        }

        public void OnAreaStay(MapObject other)
        {
            if (!other.Mobile) return;
            if (self.Alignment == other.Alignment) return;
            
            other.GetComponent<Formation>().StartCapture();
        }

        public void OnAreaExit(MapObject other)
        {
            unitsNearby.Remove(other);
        }
    }
}