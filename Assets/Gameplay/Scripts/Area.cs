using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Extensions;
using System;
using UnityEngine.Events;
using UnityEditor.UI;

namespace Gameplay
{
    [Serializable]
    public class AreaEnterEvent : UnityEvent<MapObject> { }

    [Serializable]
    public class AreaStayEvent : UnityEvent<MapObject> { }

    [Serializable]
    public class AreaExitEvent : UnityEvent<MapObject> { }


    public class Area : MonoBehaviour
    {
        [SerializeField]
        private float radius = 2f;

        [Header("Events")]
        public AreaEnterEvent AreaEnter;
        public AreaStayEvent AreaStay;
        public AreaExitEvent AreaExit;

        public float Radius { get => radius; set => radius = value; }


        /* Collisions */

        private void OnTriggerEnter(Collider other)
        {
            MapObject op = other.FindMapObject();

            if (op != null)
                AreaEnter.Invoke(op);
        }

        private void OnTriggerStay(Collider other)
        {
            MapObject op = other.FindMapObject();

            if (op != null)
                AreaStay.Invoke(op);
        }

        private void OnTriggerExit(Collider other)
        {
            MapObject op = other.FindMapObject();

            if (op != null)
                AreaExit.Invoke(op);
        }

        /* Editor */

        private void OnValidate()
        {
            // ��������� ������ ���������� ��� ������ ����
            transform.localScale = transform.localScale.withX(radius * 2).withZ(radius * 2);
        }
    }
}