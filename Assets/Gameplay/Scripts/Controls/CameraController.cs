using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class CameraController : MonoBehaviour
    {
        [SerializeField]
        private Transform cam;

        [SerializeField]
        public float panSpeed = 10;

        [SerializeField]
        public float scrollSpeed = 10;

        [SerializeField]
        public float dragSpeed = 0.1f;

        protected Vector3 direction;

        protected Vector3 lastMousePosition;

        void Update()
        {
            // Скролл и по кнопкам
            Vector3 panDirection = Vector3.zero;

            if (Input.GetKey(KeyCode.W))
                panDirection += transform.forward;

            if (Input.GetKey(KeyCode.S))
                panDirection -= transform.forward;

            if (Input.GetKey(KeyCode.D))
                panDirection += transform.right;

            if (Input.GetKey(KeyCode.A))
                panDirection -= transform.right;

            float speedMul = Input.GetKey(KeyCode.LeftShift) ? 2 : 1;

            direction = Time.deltaTime * (
                (panSpeed * speedMul * panDirection) +
                (scrollSpeed * 100 * Input.mouseScrollDelta.y * cam.forward)
            );

            // Не даём скроллить сквозь землю
            if (Physics.Raycast(
                new Ray(transform.position, direction),
                out var hit, float.MaxValue, Map.main.GroundLayer
            ))
            {
                direction = Vector3.ClampMagnitude(direction, hit.distance - 1f);
            }

            transform.Translate(direction);
            
            // Перетаскивание
            if (Input.GetMouseButtonDown(2))
            {
                lastMousePosition = Input.mousePosition;
            }
            else if (Input.GetMouseButton(2))
            {
                Vector3 delta = Input.mousePosition - lastMousePosition;

                transform.Translate(new Vector3(-delta.x, 0, -delta.y) * dragSpeed);

                lastMousePosition = Input.mousePosition;
            }
        }
    }
}