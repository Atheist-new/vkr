using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class SelectionSystem : MonoBehaviour
    {
        [SerializeField]
        [ReadOnly]
        private List<Selectable> selectables = new List<Selectable>();

        public void Register(Selectable selectable)
        {
            selectables.Add(selectable);
        }

        public void Unregister(Selectable selectable)
        {
            selectables.Remove(selectable);
        }

        public void ClearSelection()
        {
            selectables.ForEach(s => s.Deselect());
        }
    }
}