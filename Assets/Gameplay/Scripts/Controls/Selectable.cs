using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace Gameplay
{
    public class Selectable : MonoBehaviour
    {
        [SerializeField]
        protected GameObject Indicator;

        [SerializeField]
        protected UnityEvent OnSelected;

        [SerializeField]
        [ReadOnly]
        protected bool selected = false;
        public bool Selected { get => selected; }

        [SerializeField]
        [ReadOnly]
        protected bool actionAllowed;
        public bool ActionAllowed { get => actionAllowed; }

        protected SelectionSystem system;

        protected virtual void Start()
        {
            system = FindObjectOfType<SelectionSystem>();

            system.Register(this);

            Deselect(true);
        }

        protected virtual void OnDestroy()
        {
            system.Unregister(this);
        }

        protected virtual void OnMouseDown()
        {
            system.ClearSelection();
            Select();
        }

        public void Select()
        {
            if (!selected)
            {
                selected = true;

                StartCoroutine(AllowAction());

                if (Indicator != null)
                    Indicator.SetActive(true);

#if UNITY_EDITOR
                Selection.activeGameObject = gameObject;
#endif
            }
        }

        public void Deselect(bool force = false)
        {
            if (selected || force)
            {
                selected = false;
                actionAllowed = false;

                if (Indicator != null)
                    Indicator.SetActive(false);
            }
        }

        protected IEnumerator AllowAction()
        {
            yield return new WaitForSeconds(0.1f);

            actionAllowed = true;
        }
    }
}