using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Gameplay
{
    public class Dragable : Selectable
    {
        [SerializeField]
        private UnityEvent<MapObject> DraggedOn;

        [SerializeField]
        private UnityEvent<Vector3> DraggedTo;

        [SerializeField]
        [ReadOnly]
        private bool dragging;
        public bool Dragging => dragging;

        protected override void OnMouseDown()
        {
            dragging = true;
        }

        protected void OnMouseUp()
        {
            dragging = false;

            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, float.MaxValue, Map.main.HandleLayer.value) 
                && hit.transform.gameObject == gameObject)
            {
                base.OnMouseDown();
            }
            else if (Physics.Raycast(ray, out hit, float.MaxValue, Map.main.ObjectLayer.value))
            {
                GameObject target = hit.transform.gameObject;

                if (target == gameObject)
                    base.OnMouseDown();
                else
                    DraggedOn.Invoke(target.GetComponent<MapObject>());
            }
            else if (Physics.Raycast(ray, out hit, float.MaxValue, Map.main.GroundLayer.value))
            {
                DraggedTo.Invoke(hit.point);
            }
        }
    }
}