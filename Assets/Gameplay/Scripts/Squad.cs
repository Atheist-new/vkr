using Extensions;
using Gameplay.Pathing;
using UnityEngine;

namespace Gameplay
{
    public class Squad : Formation
    {
        [Header("Обнаружение")]
        [SerializeField]
        private Area ViewArea;

        [SerializeField]
        private Area AttackArea;
        
        [Header("Параметры")]
        [SerializeField]
        protected float health = 100f;

        [SerializeField]
        protected float damage = 10f;

        [SerializeField]
        protected float speed = .7f;

        [SerializeField]
        protected float attackSpeed = 1f;

        [SerializeField]
        protected float attackRange = 2f;

        [SerializeField]
        protected float interactionRange = 1f;

        [SerializeField]
        protected float viewRange = 5f;

        [SerializeField]
        protected float armor = 10f;

        [Header("Unit Count")]
        [SerializeField] 
        protected int unitCount = 10;

        [SerializeField] [ReadOnly]
        protected int lightlyWounded = 0;

        [SerializeField] [ReadOnly]
        protected int heavilyWounded = 0;

        [SerializeField] [ReadOnly]
        protected int dead = 0;

        [Header("Healing")]
        [SerializeField] 
        protected bool healDead = false;
        
        protected int availableUnits { get => unitCount - lightlyWounded - heavilyWounded - dead; }

        public override bool CanFight => availableUnits > 0;
        public override bool NeedsHealing => healDead ? 
            heavilyWounded + dead > availableUnits / 2 : 
            heavilyWounded > availableUnits / 2;

        protected void Awake()
        {
            statBlock.DictStat["Health"] = health;
            statBlock.DictStat["Damage"] = damage;
            statBlock.DictStat["Speed"] = speed;
            statBlock.DictStat["AttackSpeed"] = attackSpeed;
            statBlock.DictStat["AttackRange"] = attackRange;
            statBlock.DictStat["InteractionRange"] = interactionRange;
            statBlock.DictStat["ViewRange"] = viewRange;
            statBlock.DictStat["Armor"] = armor;
            
            ViewArea.Radius = ViewRange;
            AttackArea.Radius = AttackRange;
        }

        protected void Update()
        {
            healthBar.SetProgress(availableUnits, unitCount);

            if (TryGetComponent(out GraphTargeter t))
            {
                t.Inverse = NeedsHealing;
            }
        }

        public override bool OnTakeDamage(float totalDamage)
        {
            // TODO убрать волшебные числа

            while (totalDamage > 0 && availableUnits > 0)
            {
                int check = SurvivalCheck();

                if (check == 0)
                {
                    totalDamage -= 0.4f * Health;
                    lightlyWounded++;
                }
                else if (check == 1)
                {
                    totalDamage -= 0.7f * Health;
                    heavilyWounded++;
                }
                else
                {
                    totalDamage -= Health;
                    dead++;
                }
            }

            if (availableUnits == 0) 
                return true;

            return false;
        }

        public override float DealDamage(float defense)
        {
            return (Damage * AttackSpeed * availableUnits) / defense;
        }

        // TODO нормальное имя для этой функции
        private static int SurvivalCheck()
        {
            // TODO убрать волшебные числа

            float chance = Random.value;

            if (chance <= 0.33) return 0; // Легкораненый
            if (chance <= 0.66) return 1; // Тяжелораненый
            return 2; // Мёртвый
        }

        public override void OnBattleEnded()
        {
            state = FormationState.Free;

            if (availableUnits <= 0)
            {
                Destroy(gameObject);
                return;
            }

            // Починили легкораненых
            lightlyWounded = 0;

            // Сбрасывает текущую цель, чтобы нам гарантированно назначилась новая и мы к ней шли
            // Выглядит как костыль, есть ли вариант по лучше?
            GetComponent<Pathfinder>().ClearTarget(reached: false);
        }

        public override string ToString()
        {
            if (this == null) 
                return base.ToString();

            MapObject self = this.mapObject();

            string alignment = "Neutral";

            if (self.Alignment == 1)
                alignment = "Red";
            else if (self.Alignment == 2)
                alignment = "Blue";
            else if (self.Alignment == 3)
                alignment = "Green";

            return $"{alignment} squad";
        }

        public void Randomize()
        {
            unitCount = Random.Range(10, 21);
        }

        public override void Heal()
        {
            heavilyWounded = 0;

            if (healDead)
                dead = 0;
        }
    }
}