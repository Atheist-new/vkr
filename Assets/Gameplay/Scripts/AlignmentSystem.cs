using Gameplay;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class AlignmentSystem : MonoBehaviour
    {
        [SerializeField]
        private List<Material> materials = new();

        [SerializeField]
        private int userAlignment = 1;

        public int UserAlignment { get => userAlignment; }

        public Material GetMaterial(int alignment)
        {
            return materials[alignment];
        }

        private static AlignmentSystem instance;
        public static AlignmentSystem main { get => instance; }

        private void Awake()
        {
            instance = this;
        }
    }
}