using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Extensions;
using UnityEditor;
using UnityEngine;

namespace Gameplay
{
    public enum PositionType
    {
        Front1, Front2, Front3, Front4, Front5,
        FlankLeftFront, FlankRightFront,
        FlankLeftBack,  FlankRightBack,
    }

    [Serializable]
    public class ArmyPosition
    {
        public PositionType type;
        public Transform transform;
    }
    
    public class Army : Formation
    {
        [SerializeField]
        private List<ArmyPosition> positions;

        [SerializeField]
        private List<Squad> squads;

        public void Start()
        {
            var stats = new List<string>
            {
                "Health",
                "Damage",
                "Speed",
                "AttackSpeed",
                "AttackRange",
                "InteractionRange",
                "ViewRange",
                "Armor",
            };

            foreach (var stat in stats)
            {
                statBlock.DictStat[stat] = squads.Min(squad => squad.GetStat(stat));
            }

            foreach (var squad in squads)
            {
                squad.mapObject().Align(this.mapObject().Alignment);
                squad.State = FormationState.Marching;
            }
        }

        public void OnDrawGizmos()
        {
            foreach (var pos in positions)
            {
                var position = pos.transform == null ? transform.position : pos.transform.position;
                
                Handles.color = Color.white;
                Handles.DrawWireDisc(position, Vector3.up, 0.25f);
            }
        }
    }
}