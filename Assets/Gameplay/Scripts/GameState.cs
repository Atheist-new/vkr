using System.Collections;
using System.Collections.Generic;
using Gameplay;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Gameplay
{
    public class GameState : MonoBehaviour
    {
        [SerializeField]
        private GameObject endPanel;

        [SerializeField]
        private GameObject youWinText;

        [SerializeField]
        private GameObject gameOverText;

        [SerializeField]
        private GameObject menuButton;

        public void OnMainBuildingCaptured(int alignment)
        {
            endPanel.SetActive(true);
            menuButton.SetActive(true);
        
            if (alignment == AlignmentSystem.main.UserAlignment)
            {
                youWinText.SetActive(true);
            }
            else
            {
                gameOverText.SetActive(true);
            }
        
        
        }

        public void OnMainMenu()
        {
            SceneManager.LoadScene("Menu");
        }
    }
}
