using Extensions;
using System.Collections;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Gameplay.Pathing;

namespace Gameplay
{
    public class Agent : MonoBehaviour
    {
        private const float UPDATE_RATE = .033f;

        private Path path = null;

        private Pathfinder pathfinder;
        private MapObject mapObject;
        private Formation formation;

        [SerializeField]
        private Transform rotatable; // TODO нормальное название

        [SerializeField]
        private float rotationThreshold = 15f;  // Максимальный угол на который можно вращаться в движении
        
        private Coroutine followCoro = null;

        private Vector3 Position { get => transform.position.withY(0); }

        // Start is called before the first frame update
        void Start()
        {
            pathfinder = GetComponent<Pathfinder>();
            mapObject = GetComponent<MapObject>();
            formation = GetComponent<Formation>();
        }

        // Update is called once per frame
        void Update()
        {
            if (pathfinder.TargetChanged)
                if (pathfinder.Target != null)
                    StartCoroutine(UpdatePath());
        }

        private IEnumerator UpdatePath()
        {
            var pos = transform.position;

            path = new();

            yield return pathfinder.GetPath(pos, path);

            if (path.Points != null)
            {
                if (followCoro != null)
                    StopCoroutine(followCoro);

                followCoro = StartCoroutine(FollowPath());
            }
            else
            {
                if (followCoro != null)
                    StopCoroutine(followCoro);

                followCoro = null;

                pathfinder.ClearTarget(reached: false);
            }
        }

        private IEnumerator FollowPath()
        {
            var walker = new PathWalker(path);

            float elapsed = 0;

            while (true)
            {
                if (formation.ShouldMove)
                {
                    if (pathfinder.Target == null)
                    {
                        followCoro = null;
                        yield break;
                    }

                    MapObject target = pathfinder.Target.Object;

                    if (target == null)
                    {
                        pathfinder.ClearTarget(reached: false);
                        yield break;
                    }

                    elapsed += UPDATE_RATE;
                    if (elapsed >= 1)
                    {
                        elapsed -= 1;

                        if (target.Mobile)
                        {
                            StartCoroutine(UpdatePath());
                        }
                    }
                    
                    // TODO Вынести в StatBlock как радиус атаки
                    float radius = (
                        target.Alignment == 0 || target.Alignment == mapObject.Alignment
                    ) ? formation.InteractionRange : formation.AttackRange;

                    if (!target.Passable && Vector3.Distance(Position, target.Position) < radius)
                    {
                        pathfinder.ClearTarget(reached: true);
                        yield break;
                    }

                    var point = walker.Walk(Map.main.ToWorldSpace(formation.Speed) * UPDATE_RATE);
                    if (!UpdatePosition(point))
                    {
                        walker.Undo();
                    }
                    
                    if (walker.Finished)
                    {
                        pathfinder.ClearTarget(reached: true);
                        yield break;
                    }
                }

                yield return new WaitForSeconds(UPDATE_RATE);
            }
        }

        private Vector3 _direction = Vector3.forward;
        
        private bool UpdatePosition(PathPoint point)
        {
            _direction = point.RelTan.normalized;
            var angle = Vector3.Angle(rotatable.forward, _direction);
            var walk = angle < rotationThreshold;
            
            if (walk)
            {
                transform.position = point.Pos;
            }

            if (angle > 1f)
            {
                rotatable.rotation = Quaternion.RotateTowards(
                    rotatable.rotation, Quaternion.LookRotation(_direction),
                    formation.RotationSpeed * UPDATE_RATE
                );
            }

            return walk;
        }

        private void OnDrawGizmos()
        {
            var position = rotatable.position;
            var forward = rotatable.forward;
            
            Handles.DrawBezier(
                position, position + _direction, 
                position, position + _direction,
                Color.magenta, null, 3
            );
            
            Handles.DrawBezier(
                position, position + forward,
                position, position + forward,
                Color.red, null, 3
            );
        }

        private void OnDrawGizmosSelected()
        {
            if (path == null) return;

            var worldSpaced = path?.Points?.ToList();
            // List<(Vector3 pos, Vector3 tan, float len)> v = new() {(pos, pos, 0)};
            
            worldSpaced?
            .Slide2()
            .Apply(t =>
                {
                    var (a, b) = t;
                    Handles.DrawBezier(
                        a.Pos, b.Pos,
                        a.Tan, b.InvTan,
                        Color.yellow, null, 5
                    );
                    
                    // v.Add((b.pos, b.tan, (b.pos - a.pos).magnitude));
                    
                    Handles.color = Color.red;
                    Handles.DrawLine(a.Pos, a.Tan);
                    Handles.color = Color.blue;
                    Handles.DrawLine(b.Pos, b.InvTan);
                    Handles.color = Color.white;
                }
            );
            
            // Debug.Log(string.Join('\n', v.Select(item => $"{item.pos} ({item.len}) {item.tan}")));
        }
    }
}