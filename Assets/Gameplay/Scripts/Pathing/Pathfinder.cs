using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

namespace Gameplay.Pathing
{
    public class Pathfinder : MonoBehaviour
    {
        protected Target target = null;
        public Target Target => target;

        [SerializeField]
        protected NavMeshAgentType agentType;
        
        [SerializeField]
        [ReadOnly]
        protected bool targetChanged = false;
        public bool TargetChanged => targetChanged; 

        private MapObject mapObject => GetComponent<MapObject>();

        public void SetTarget(Target t)
        {
            if (target != null)
                target.OnTargetCanceled();

            target = t;
            targetChanged = true;

            target.OnTargetSet();
        }
        public Target GetTarget()
        {
            return target;
        }
        public void ClearTarget(bool reached = false)
        {
            if (target != null)
            {
                if (reached) 
                    target.OnTargetReached();
                else 
                    target.OnTargetCanceled();
            }    
                

            target = null;
            targetChanged = true;
        }

        public IEnumerator GetPath(Vector3 from, Path path)
        {
            targetChanged = false;

            if (target == null || target.Object == null)
                yield break;

            target.Object.DisableObstacle();
            mapObject.DisableObstacle();

            yield return null;  // Ждем 1 фрейм чтобы навмеш успел обновиться

            if (target == null || target.Object == null)
                yield break;

            NavMeshPath nmPath = new();

            var surface = Map.main.GetNavMeshFor(agentType);

            if (
                NavMesh.CalculatePath(
                    from,
                    target.Position,
                    new NavMeshQueryFilter { agentTypeID = surface.agentTypeID, areaMask = NavMesh.AllAreas },
                    nmPath
                )
            )
            {
                path.SetPoints(nmPath.corners.Select(Map.AdjustHeight));
            }

            target.Object.EnableObstacle();
            mapObject.EnableObstacle();
        }

        private void OnDestroy()
        {
            // Удалить флаг, если объект разрушен
            if (target != null)
                target.OnTargetRemoved();
        }
    }
}