﻿using UnityEngine;

namespace Gameplay.Pathing
{
    // Точка кривой Безье
    public class PathPoint
    {
        public Vector3 Pos;
        public Vector3 Tan;
        
        public Vector3 InvTan
        {
            get => 2 * Pos - Tan;
            set => Tan = 2 * Pos - value;
        }

        public Vector3 RelTan
        {
            get => Tan - Pos;
            set => Tan = Pos + value;
        }

        public Vector3 RelInvTan
        {
            get => InvTan - Pos;
            set => InvTan = Pos + value;
        }

        public PathPoint() {}
        public PathPoint(Vector3 v) { Pos = v; Tan = v; }
        public PathPoint(Vector3 p, Vector3 t) { Pos = p; Tan = t; }
        
        public static implicit operator PathPoint(Vector3 v) => new(v);
        public static implicit operator Vector3(PathPoint p) => p.Pos;
        
        public override string ToString()
        {
            return Pos.ToString();
        }
    }
}