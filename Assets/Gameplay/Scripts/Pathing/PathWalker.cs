﻿namespace Gameplay.Pathing
{
    public class PathWalker
    {
        public Path Path { get; }
        public int Current = 0;
        public PathSegment Segment => Path.Segments[Current];

        private float _distanceWalked = 0;
        public bool Finished { get; private set; } = false;

        private (int current, float distance) _previous = (0, 0);

        public PathWalker(Path path)
        {
            Path = path;
        }

        public PathPoint Walk(float distance)
        {
            _previous = (Current, _distanceWalked);
            
            _distanceWalked += distance;

            if (_distanceWalked < Segment.Length) 
                return Segment.LenLerp(_distanceWalked);
            
            var remainder = _distanceWalked - Segment.Length;
            Current += 1;

            if (Current < Path.Segments.Count)
            {
                // !!!
                _distanceWalked = 0;
                return Walk(remainder);
            }
            
            Finished = true;
            return Path.Segments[^1].End;
        }

        public void Undo()
        {
            (Current, _distanceWalked) = _previous;
        }
    }
}