﻿using System.Collections.Generic;
using System.Linq;
using Extensions;
using UnityEngine;

namespace Gameplay.Pathing
{
    // Путь в виде кривой безье
    public class Path
    {
        // Масштаб тангенс-векторов
        public const float Scale = 0.5f;
        // Минимальное расстояние между соседними точками пути
        public const float MinDistance = 0.2f;
        // Количество вспомогательных точек для интерполяции по длине
        public const int Resolution = 100;
        
        public List<PathPoint> Points { get; private set; } = null;
        public List<PathSegment> Segments { get; private set; } = null;

        public void SetPoints(IEnumerable<Vector3> points)
        {
            Points = points.Append(Vector3.zero).Slide2()
                // Убрать точки, слишком близко расположенные к соседним
                .Where(pair => (pair.Item1 - pair.Item2).magnitude > MinDistance)
                // Конверитровать в путевые точки
                .Select(pair => new PathPoint(pair.Item1)).ToList();
            
            MakeTangents();
            
            Segments = Points.Slide2().Select(
                pp => new PathSegment(pp, Resolution)
            ).ToList();
        }
        
        // Рассчитать тангенсы для точек пути
        public void MakeTangents()
        {
            if (Points.Count >= 2)
            {
                Points[0].RelTan = (Points[1].Pos - Points[0].Pos).normalized * Scale;
                Points[^1].RelTan = (Points[^1].Pos - Points[^2].Pos).normalized * Scale;
            }

            Points.Slide3().Apply(t =>
            {
                var (p0, p1, p2) = t;
                var d0 = p1.Pos - p0.Pos;
                var d1 = p2.Pos - p1.Pos;
                p1.RelTan = ((d0 + d1) / 2).normalized * Scale;
            });
            
        }

        public override string ToString()
        {
            return "{" + string.Join(", ", Points.Select(v => v.ToString())) + "}";
        }
    }
}