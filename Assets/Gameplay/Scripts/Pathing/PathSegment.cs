﻿using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using Extensions;

namespace Gameplay.Pathing
{
    // Сегмент кривой Безье, умеет равномерно интерполировать точки на кривой
    public class PathSegment
    {
        public readonly PathPoint Start;
        public readonly PathPoint End;

        private List<float> _partitions;

        // Количество опорных точек для интерполяции
        public int Resolution { get; private set; }
        // Расстояние между опорными точками (по коэффициенту t)
        public float Delta => 1f / Resolution;
        // Длина сегмента (в условных единицах)
        public float Length => _partitions[^1];

        public PathSegment(PathPoint start, PathPoint end, int resolution = 0)
        {
            Start = start;
            End = end;

            Subdivide(resolution);
        }
        
        public PathSegment((PathPoint start, PathPoint end) pair, int resolution = 0)
            : this(pair.start, pair.end, resolution) { }

        // Нужно для автоматической распаковки сегмента на концы
        public void Deconstruct(out PathPoint start, out PathPoint end)
        {
            start = Start;
            end = End;
        }
        
        // Рассчитать опорные точки
        public void Subdivide(int resolution = 0)
        {
            Resolution = resolution == 0 ? 100 : resolution;
            
            _partitions = new List<float>();

            var previous = Start.Pos;
            var length = 0f;

            for (var i = 0; i <= Resolution; i++)
            {
                var pos = PosAt(i * Delta);
                length += (pos - previous).magnitude;
                _partitions.Add(length);

                // !!!!!!!!!!!!
                previous = pos;
            }
        }
        
        public Vector3 PosAt(float t)
        {
            return Start.Pos * math.pow(1 - t, 3) +
                   Start.Tan * (3 * math.pow(1 - t, 2) * t) +
                   End.InvTan * (3 * (1 - t) * math.pow(t, 2)) +
                   End.Pos * math.pow(t, 3);
        }

        public Vector3 TanAt(float t)
        {
            return Start.Pos * (-math.pow(1 - t, 2)) +
                   Start.Tan * (t * (3 * t - 4) + 1) +
                   End.InvTan * (-3 * math.pow(t, 2) + 2 * t) +
                   End.Pos * math.pow(t, 2);
        }
        
        public PathPoint PointAt(float t)
        {
            var pos = PosAt(t);
            var tan = TanAt(t);

            return new PathPoint {Pos = pos, Tan = pos + tan};
        }
        
        // Значение t для точки с длиной дуги fraction * Length
        public float T(float fraction)
        {
            var len = fraction * Length;
            
            var (a, b) = _partitions.FindBounds(len);
            return math.remap(_partitions[a], _partitions[b], Delta * a, Delta * b, len);
        }
        
        // Значение t для точки с длиной дуги len
        public float TLen(float len)
        {
            var (a, b) = _partitions.FindBounds(len);
            return math.remap(_partitions[a], _partitions[b], Delta * a, Delta * b, len);
        }

        // Равномерная интерполяция вдоль кривой
        public PathPoint BLerp(float t)
        {
            return PointAt(T(t));
        }

        public PathPoint LenLerp(float len)
        {
            return PointAt(TLen(len));
        }
    }
}