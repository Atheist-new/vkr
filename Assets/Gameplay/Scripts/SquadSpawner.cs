using Gameplay;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace Gameplay
{
    public class SquadSpawner : MonoBehaviour
    {
        [Header("Настройки")]
        [SerializeField]
        private float spawnDelay = 10;

        [SerializeField]
        private float spawnRadius = 2;

        [SerializeField]
        private GameObject squadPrefab;

        [SerializeField]
        private Transform spawnPoint;

        [SerializeField]
        private ProgressBar progressBar;

        [SerializeField] [ReadOnly]
        private float elapsed = 0;

        private MapObject self;

        private void Start()
        {
            elapsed = spawnDelay - 1;
            self = GetComponent<MapObject>();

            progressBar.SetProgress(elapsed, spawnDelay);
        }

        private void Update()
        {
            if (self.Alignment == 0)
                return;

            elapsed += Time.deltaTime;

            if (elapsed > spawnDelay)
            {
                elapsed -= spawnDelay;

                SpawnSquad();
            }

            progressBar.SetProgress(elapsed, spawnDelay);
        }

        private void SpawnSquad()
        {
            Vector3 pos = transform.position + Vector3.right * Map.main.ToWorldSpace(spawnRadius);

            MapObject squad = Map.main.CreateObject(squadPrefab, pos);
            squad.Align(self.Alignment);

            // TODO убрать это когда появятся нормальные статы
            squad.GetComponent<Squad>().Randomize();

            GraphTargeter t = squad.GetComponent<GraphTargeter>();
            t.Current = GetComponent<GraphNode>();
        }
    }
}