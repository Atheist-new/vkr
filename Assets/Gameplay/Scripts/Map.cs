using System;
using System.Collections;
using System.Collections.Generic;
using Unity.AI.Navigation;
using UnityEngine;
using UnityEngine.AI;

namespace Gameplay
{
    public enum NavMeshAgentType
    {
        Squad, Army
    }
    
    [Serializable]
    public class NMPair
    {
        public NavMeshAgentType type;
        public NavMeshSurface surface;
    }
    
    public class Map : MonoBehaviour
    {
        private static Map instance;
        public static Map main { get => instance; }

        [SerializeField]
        private float scale = 1f;

        [SerializeField]
        private LayerMask groundLayer;

        [SerializeField]
        private LayerMask objectLayer;

        [SerializeField]
        private LayerMask handleLayer;
        
        [SerializeField]
        private GameObject FlagPrefab;

        [SerializeField]
        private Transform objectsRoot;

        [SerializeField]
        private List<NMPair> navMeshes;

        public LayerMask GroundLayer => groundLayer;
        public LayerMask ObjectLayer => objectLayer;
        public LayerMask HandleLayer => handleLayer;

        public Vector3 Origin => transform.position;

        private void Awake()
        {
            instance = this;
        }

        public NavMeshSurface GetNavMeshFor(NavMeshAgentType type)
        {
            return navMeshes.Find(pair => pair.type == type).surface;
        }

        public static float GetHeightAt(Vector3 pos)
        {
            return Terrain.activeTerrain.SampleHeight(pos);
        }

        public static Vector3 AdjustHeight(Vector3 pos)
        {
            return new Vector3(pos.x, Terrain.activeTerrain.SampleHeight(pos), pos.z);
        }
        
        public float ToWorldSpace(float distance)
        {
            return distance / scale;
        }

        public float ToMapSpace(float distance)
        {
            return distance * scale;
        }

        public MapObject CreateFlag(Vector3 pos)
        {
            return CreateObject(FlagPrefab, pos);
        }
        
        public MapObject CreateObject(GameObject prefab, Vector3 pos) 
        {
            // TODO place object at the ground
            var newObject = Instantiate(prefab, pos, Quaternion.identity, objectsRoot);
            return newObject.GetComponent<MapObject>();
        }
    }
}