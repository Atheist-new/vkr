using Gameplay;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/* Это костыль чтобы компилились проперти с init */
namespace System.Runtime.CompilerServices
{
    internal static class IsExternalInit { }
}

namespace Extensions
{
    public static class Randomizer
    {
        public static System.Random r { get; }

        static Randomizer()
        {
            r = new System.Random();
        }
    }
    
    public static class VectorExtensions
    {
        public static Vector3 withX(this Vector3 v, float x) => new(x, v.y, v.z);
        public static Vector3 withY(this Vector3 v, float y) => new(v.x, y, v.z);
        public static Vector3 withZ(this Vector3 v, float z) => new(v.x, v.y, z);
        
        public static Vector2 ToVector2(this Vector3 v) => new(v.x, v.z);
        public static Vector3 ToVector3(this Vector2 v) => new(v.x, 0, v.y);
        
        public static Vector2 RandomOffset(this float radius)
        {
            var angle = Randomizer.r.Next(360);
            return (
                Quaternion.AngleAxis(angle, Vector3.up) *   // Поворот на angle градусов
                Vector3.forward *                           // Поворачиаемый угол
                (float)Randomizer.r.NextDouble()            // Рандомная длина
            ).ToVector2();
        }
    }

    public static class UnityExtensions 
    {
        public static MapObject FindMapObject(this GameObject gameObject)
        {
            if (gameObject.TryGetComponent<MapObject>(out var mo))
                return mo;

            if (gameObject.transform.parent != null)
                return gameObject.transform.parent.FindMapObject();

            return null;
        }

        public static MapObject FindMapObject(this Component component)
        {
            return component.gameObject.FindMapObject();
        }
        public static MapObject mapObject(this Component component)
        {
            component.TryGetComponent<MapObject>(out var mo);
            return mo;
        }
    }

    public static class EnumerableExtensions
    {
        // Возвращает все элементы последовательности, для которых comparer возвращает максимальное значение
        public static IEnumerable<T> MaxAll<T, TResult>(this IEnumerable<T> sequence, Func<T, TResult> predicate)
        {
            var max = sequence.Max(predicate);

            if (max == null) return Enumerable.Empty<T>();

            return sequence.Where(item => predicate(item).Equals(max));
        }

        // Возвращает случайный элемент последовательности
        public static T Random<T>(this IEnumerable<T> sequence)
        {
            return sequence.ElementAt(Randomizer.r.Next(sequence.Count()));
        }

        // Попарно выбирает соседние элементы последовательности 
        // (0, 1) (1, 2) (2, 3) ...
        public static IEnumerable<(T, T)> Slide2<T>(this IEnumerable<T> seq)
        {
            using var iter = seq.GetEnumerator();
            
            if (!iter.MoveNext())
                yield break;

            var prev = iter.Current;

            while (iter.MoveNext())
            {
                var next = iter.Current;
                yield return (prev, next);
                prev = next;
            }
        }
        
        // Выбирает соседние элементы последовательности по 3
        // (0, 1, 2) (1, 2, 3) (2, 3, 4) ...
        public static IEnumerable<(T, T, T)> Slide3<T>(this IEnumerable<T> seq)
        {
            using var iter = seq.GetEnumerator();
            
            if (!iter.MoveNext())
                yield break;

            var prev = iter.Current;
            
            if (!iter.MoveNext())
                yield break;

            var curr = iter.Current;

            while (iter.MoveNext())
            {
                var next = iter.Current;
                yield return (prev, curr, next);
                prev = curr;
                curr = next;
            }
        }

        // Применяет функцию к каждому элементу последовательности
        public static void Apply<T>(this IEnumerable<T> sequence, Action<T> function)
        {
            foreach (var element in sequence)
            {
                function(element);
            }
        }

        public static T Pop<T>(this List<T> list)
        {
            var item = list[^1];
            list.RemoveAt(list.Count - 1);
            return item;
        }
        
        public static IEnumerable<(int, T)> Indexed<T>(this IEnumerable<T> list) => list.Select((v, i) => (i, v));
        
        // Бинарный поиск элементов в списке между которыми находится target
        public static (int, int) FindBounds<T>(this List<T> list, T target)
        {
            var cmp = Comparer<T>.Default;

            var a = -1;
            var b = -1;

            if (list.Count == 0)
            {
                return (a, b);
            }

            var left = 0;
            var right = list.Count - 1;

            // Perform binary search to find the bounds
            while (left <= right)
            {
                var mid = left + (right - left) / 2;

                switch (cmp.Compare(list[mid], target))
                {
                    case 0:
                        return (mid, mid);
                    case < 0:
                        a = mid;
                        left = mid + 1;
                        break;
                    default:
                        b = mid;
                        right = mid - 1;
                        break;
                }
            }

            // Adjust 'a' and 'b' based on the target value and list bounds
            if (cmp.Compare(target, list[0]) < 0) a = -1;
            if (cmp.Compare(list[^1], target) < 0) b = -1;
            
            return (a, b);
        }
    }
}