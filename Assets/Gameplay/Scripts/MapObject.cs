using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Gameplay
{

    public class MapObject : MonoBehaviour
    {
        [SerializeField]
        private int alignment = 0;
        public int Alignment { get => alignment; }

        [SerializeField]
        private bool mobile = false;
        public bool Mobile { get => mobile; }

        [SerializeField]
        private bool passable = false;
        public bool Passable { get => passable; }

        [SerializeField]
        private List<MeshRenderer> colorable = new();

        public Vector3 Position => transform.position;

        private NavMeshObstacle obstacle = null;

        private bool wasObstacle = false;

        protected virtual void Awake()
        {
            TryGetComponent(out obstacle);
        }

        protected virtual void Start()
        {
            UpdateMaterial();
        }

        public virtual void OnSetTarget() { }
        public virtual void OnRemoveTarget() { }

        private int _obstacleDisabled = 0;
        
        public void DisableObstacle()
        {
            if (obstacle == null) return;
            
            _obstacleDisabled += 1;
            obstacle.carving = false;
        }

        public void EnableObstacle()
        {
            if (obstacle == null) return;
            
            _obstacleDisabled -= 1;

            if (_obstacleDisabled == 0)
            {
                obstacle.carving = true;
            }
        }

        public void Align(int newAlignment)
        {
            alignment = newAlignment;
            UpdateMaterial();
        }

        private void UpdateMaterial()
        {
            foreach (var mr in colorable)
            {
                mr.material = AlignmentSystem.main.GetMaterial(alignment);
            }
        }
    }
}